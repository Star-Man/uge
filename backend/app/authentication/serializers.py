"""Serializers allow querysets and model instances to be converted to native Python."""

from rest_auth.registration.serializers import RegisterSerializer
from rest_auth.serializers import LoginSerializer, PasswordChangeSerializer
from rest_framework import serializers
from rest_framework.authtoken.models import Token

from . import models


class GroupSerializer(serializers.ModelSerializer):
    """View the Group model as a serializer."""

    class Meta:
        model = models.CustomGroup
        fields = ("id", "name", "isActive")


class UserSerializerGet(serializers.ModelSerializer):
    """View the User model as a serializer."""

    groups = GroupSerializer(many=True, read_only=True)
    isActive = serializers.BooleanField(source="is_active")

    class Meta:
        model = models.CustomUser
        fields = (
            "id",
            "displayName",
            "username",
            "groups",
            "isActive",
        )
        read_only_fields = ("username",)


class UserSerializer(serializers.ModelSerializer):
    """Patch the User model as a serializer."""

    isActive = serializers.BooleanField(source="is_active")

    class Meta:
        model = models.CustomUser
        fields = (
            "id",
            "username",
            "displayName",
            "groups",
            "isActive",
        )

    def to_representation(self, instance):
        """Return a nested view as a response to a POST request."""
        serializer = UserSerializerGet(instance)
        return serializer.data


class CustomRegisterSerializer(RegisterSerializer):
    """Allow a user to register and returns their JWT token."""

    password1 = serializers.CharField(write_only=True)
    username = serializers.CharField(required=True)
    displayName = serializers.CharField(required=True)
    email = None

    def get_cleaned_data(self):
        """Clean registration data for serializer."""
        data_dict = super().get_cleaned_data()
        data_dict["displayName"] = self.validated_data.get(  # pylint: disable=E1101
            "displayName", ""
        )
        return data_dict


class CustomLoginSerializer(LoginSerializer):
    """Allow a user to login and returns their JWT token."""

    email = None

    class Meta:
        model = models.CustomUser
        fields = ("username", "displayName")


class TokenSerializer(serializers.ModelSerializer):
    """Return token and user data."""

    user = UserSerializer(read_only=True)

    class Meta:
        model = Token
        fields = ("key", "user")


class AdminPasswordChangeSerializer(PasswordChangeSerializer):
    """Redefine existing PasswordChangeSerializer to not use old password."""

    userId = serializers.IntegerField(min_value=0)

    def __init__(self, *args, **kwargs):
        """Redefine init to remove old_password field."""
        super().__init__(*args, **kwargs)
        self.fields.pop("old_password")
        self.request = self.context.get("request")
        self.user = getattr(self.request, "user", None)

    def validate(self, attrs):
        """Redefine validate function to use user passed with userId."""
        try:
            selected_user = models.CustomUser.objects.get(id=attrs["userId"])
        except models.CustomUser.DoesNotExist:
            raise serializers.ValidationError("This user does not exist")
        self.set_password_form = self.set_password_form_class(
            user=selected_user, data=attrs
        )

        if not self.set_password_form.is_valid():
            raise serializers.ValidationError(self.set_password_form.errors)
        return attrs
