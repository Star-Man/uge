"""URLs routes that are part of the main django app."""

from django.urls import path
from rest_auth.views import PasswordChangeView

from . import views

urlpatterns = [  # pylint: disable=C0103
    path("users/", views.UserListView.as_view(), name="users"),
    path("groups/", views.GroupListView.as_view(), name="groups"),
    path("registration/", views.CustomRegisterView.as_view(), name="register"),
    path("login/", views.CustomLoginView.as_view(), name="login"),
    path("change-password/", PasswordChangeView.as_view(), name="change-password"),
    path(
        "change-password-admin/",
        views.AdminPasswordChangeView.as_view(),
        name="change-password-admin",
    ),
    path("users/<int:pk>/update/", views.UserUpdate.as_view(), name="user-update"),  #
    path("groups/<int:pk>/update/", views.GroupUpdate.as_view(), name="group-update"),
]
