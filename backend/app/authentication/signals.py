"""Websocket signals sent on data change."""

from asgiref.sync import async_to_sync
from channels.layers import get_channel_layer
from django.conf import settings
from django.db.models.signals import m2m_changed, post_save
from django.dispatch import receiver
from django.forms.models import model_to_dict

from .models import CustomGroup, CustomUser


def update_user_websocket(instance, groups=False):
    """Send the updated/created user to the websocket consumer."""
    serialized_obj = model_to_dict(instance)
    channel_layer = get_channel_layer()
    data = {
        "id": serialized_obj["id"],
        "username": serialized_obj["username"],
        "displayName": serialized_obj["displayName"],
        "isActive": serialized_obj["is_active"],
    }
    if groups:
        groups_list = []
        for group in instance.groups.all():
            groups_list.append(model_to_dict(group))
        data["groups"] = groups_list
    async_to_sync(channel_layer.group_send)(
        "nonAdmin", {"type": "update", "action": "updateUser", "data": data},
    )


@receiver(post_save, sender=settings.AUTH_USER_MODEL)
def save_user(
    sender, instance, created, update_fields, **kwargs
):  # pylint: disable=W0613
    """Set the first user to be an admin, and the following users to no group."""
    if created:
        if CustomUser.objects.count() <= 1:
            instance.groups.add(CustomGroup.objects.get(name="admin"))
        update_user_websocket(instance)
    elif not update_fields or not set(["last_login"]).intersection(update_fields):
        update_user_websocket(instance)


@receiver(m2m_changed, sender=CustomUser.groups.through)
def save_user_group_change(sender, instance, action, **kwargs):  # pylint: disable=W0613
    """Update websocket if user group changes."""
    if action in ("post_add", "post_remove"):
        update_user_websocket(instance, groups=True)


@receiver(post_save, sender=CustomGroup)
def save_group(
    sender, instance, created, update_fields, **kwargs
):  # pylint: disable=W0613
    """Send group data to WS."""
    serialized_obj = model_to_dict(instance)
    channel_layer = get_channel_layer()
    data = {
        "id": serialized_obj["id"],
        "name": serialized_obj["name"],
        "isActive": serialized_obj["isActive"],
    }
    async_to_sync(channel_layer.group_send)(
        "nonAdmin", {"type": "update", "action": "updateGroup", "data": data},
    )
