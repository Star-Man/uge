"""A view is a function that takes a Web request and returns a Web response."""

from rest_auth.registration.views import RegisterView
from rest_auth.views import LoginView, PasswordChangeView
from rest_framework import generics

from ..authentication.permissions import IsAdmin, IsAnyGroup, IsUserOrAdmin
from . import models, serializers


class UserListView(generics.ListAPIView):
    """Get all user objects from database and return for serializer."""

    queryset = models.CustomUser.objects.all()
    serializer_class = serializers.UserSerializer
    permission_classes = [IsAnyGroup]


class GroupListView(generics.ListCreateAPIView):
    """Get all group objects from database and return for serializer."""

    queryset = models.CustomGroup.objects.all()
    serializer_class = serializers.GroupSerializer
    permission_classes = [IsAdmin]


class CustomRegisterView(RegisterView):
    """Allow a user to register based on the CustomUser object."""

    queryset = models.CustomUser.objects.all()


class CustomLoginView(LoginView):
    """Allow a user to login based on the CustomUser object."""

    queryset = models.CustomUser.objects.all()


class UserUpdate(generics.UpdateAPIView):
    """Update User objects."""

    queryset = models.CustomUser.objects.all()
    serializer_class = serializers.UserSerializer
    permission_classes = [IsUserOrAdmin]


class GroupUpdate(generics.UpdateAPIView):
    """Update Group objects."""

    queryset = models.CustomGroup.objects.all()
    serializer_class = serializers.GroupSerializer
    permission_classes = [IsAdmin]


class AdminPasswordChangeView(PasswordChangeView):
    """Redefine existing PasswordChangeView to use AdminPasswordChangeSerializer."""

    permission_classes = [IsAdmin]
    serializer_class = serializers.AdminPasswordChangeSerializer
