"""Generic django form object for inputting data."""

from django.contrib.auth.forms import UserChangeForm, UserCreationForm

from .models import CustomUser


class CustomUserCreationForm(UserCreationForm):
    """Form for creating a custom user."""

    class Meta:
        model = CustomUser
        fields = ("username", "displayName")


class CustomUserChangeForm(UserChangeForm):
    """Form for changing a custom user."""

    class Meta:
        model = CustomUser
        fields = UserChangeForm.Meta.fields
