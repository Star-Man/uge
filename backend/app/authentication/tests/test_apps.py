"""apps.py Tests."""

from django.apps import apps
from django.test import TestCase

from ..apps import AuthenticationAppConfig


class AuthenticationAppConfigTestCase(TestCase):
    """Tests for the authentication app configuration."""

    def test_name(self):
        """Tests name is correct."""
        self.assertEqual(AuthenticationAppConfig.name, "app.authentication")
        self.assertEqual(
            apps.get_app_config("authentication").name, "app.authentication"
        )
