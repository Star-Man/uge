"""Profile Model Tests."""

from django.test import TestCase

from ..models import CustomUser


class CustomUserTestCase(TestCase):
    """Tests for the CustomUser model."""

    def test_creating_user_object(self):
        """Custom User object should create successfully."""
        test_user = CustomUser.objects.create(username="testUser")
        test_user.is_active = True
        test_user.save()
        self.assertEqual(test_user.username, "testUser")
        self.assertEqual(test_user.is_active, True)

    def test_first_user_added_to_admin_group(self):
        """First user created should be in the admin group."""
        test_user = CustomUser.objects.create(username="testUser")
        test_user.is_active = True
        test_user.save()
        self.assertEqual(len(test_user.groups.all()), 1)
        self.assertEqual(test_user.groups.all()[0].name, "admin")
