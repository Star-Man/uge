"""Serializers Tests."""

from collections import OrderedDict

from django.test import TestCase
from rest_framework.exceptions import ErrorDetail
from rest_framework.test import APIClient

from ..models import CustomUser

REGISTER_URL = "/api/registration/"
TOKEN_AUTH = "Token "
DISPLAY_NAME = "Test User"
DISPLAY_NAME_2 = "Test User 2"


class CustomRegisterSerializerTestCase(TestCase):
    """Tests for CustomRegisterSerializer."""

    url = "/api/registration/"

    def test_valid(self):
        """Test sending a valid post request to the serializer."""
        response = self.client.post(
            self.url,
            {
                "username": "testUser",
                "displayName": DISPLAY_NAME,
                "password1": "testPassword",
                "password2": "testPassword",
            },
        )
        self.assertEqual(response.status_code, 201)
        self.assertIn("key", response.data)
        self.assertIn("id", response.data["user"])
        self.assertEqual(response.data["user"]["username"], "testUser")
        self.assertEqual(response.data["user"]["displayName"], DISPLAY_NAME)
        self.assertEqual(
            response.data["user"]["groups"],
            [OrderedDict([("id", 1), ("name", "admin"), ("isActive", True)])],
        )

    def test_invalid_password(self):
        """Test sending an invalid password to the serializer."""
        response = self.client.post(
            self.url,
            {
                "username": "testUser",
                "displayName": DISPLAY_NAME,
                "password1": "aa",
                "password2": "aa",
            },
        )
        self.assertEqual(response.status_code, 400)
        self.assertEqual(
            response.data,
            {
                "password1": [
                    ErrorDetail(
                        string="Password must be a minimum of 6 characters.",
                        code="invalid",
                    )
                ]
            },
        )

    def test_missing_data(self):
        """Test missing data being sent to serializer."""
        response = self.client.post(self.url, {},)
        self.assertEqual(response.status_code, 400)
        error = "This field is required."
        self.assertEqual(
            response.data,
            {
                "username": [ErrorDetail(string=error, code="required")],
                "displayName": [ErrorDetail(string=error, code="required")],
                "password1": [ErrorDetail(string=error, code="required")],
                "password2": [ErrorDetail(string=error, code="required")],
            },
        )


class CustomLoginSerializerTestCase(TestCase):
    """Tests for CustomLoginSerializer."""

    url = "/api/login/"

    def setUp(self):
        """Create a user for auth."""
        self.client.post(
            REGISTER_URL,
            {
                "username": "testUser1",
                "displayName": DISPLAY_NAME,
                "password1": "testPassword",
                "password2": "testPassword",
            },
        )

    def test_valid_post(self):
        """Test sending a valid post request to the serializer."""
        response = self.client.post(
            self.url, {"username": "testUser1", "password": "testPassword"},
        )
        self.assertEqual(response.status_code, 200)
        self.assertIn("key", response.data)
        self.assertIn("id", response.data["user"])
        self.assertEqual(response.data["user"]["username"], "testUser1")
        self.assertEqual(response.data["user"]["displayName"], DISPLAY_NAME)
        self.assertEqual(
            response.data["user"]["groups"],
            [OrderedDict([("id", 1), ("name", "admin"), ("isActive", True)])],
        )

    def test_invalid_password(self):
        """Test sending an invalid password to the serializer."""
        response = self.client.post(
            self.url, {"username": "testUser1", "password": "incorrectPassword"},
        )
        self.assertEqual(response.status_code, 400)
        self.assertEqual(
            response.data,
            {
                "non_field_errors": [
                    ErrorDetail(
                        string="Unable to log in with provided credentials.",
                        code="invalid",
                    )
                ]
            },
        )

    def test_missing_data(self):
        """Test missing data being sent to serializer."""
        response = self.client.post(self.url, {},)
        self.assertEqual(response.status_code, 400)
        self.assertEqual(
            response.data,
            {
                "password": [
                    ErrorDetail(string="This field is required.", code="required")
                ]
            },
        )

        response = self.client.post(self.url, {"password": "incorrectPassword"},)
        self.assertEqual(response.status_code, 400)
        self.assertEqual(
            response.data,
            {
                "non_field_errors": [
                    ErrorDetail(
                        string='Must include "username" and "password".', code="invalid"
                    )
                ]
            },
        )


class UserSerializer(TestCase):
    """Tests for UserSerializer."""

    url = "/api/users/"
    user = None

    def setUp(self):
        """Create a user for auth."""
        self.client = APIClient()
        self.user = self.client.post(
            REGISTER_URL,
            {
                "username": "testUser1",
                "displayName": DISPLAY_NAME,
                "password1": "testPassword",
                "password2": "testPassword",
            },
        )
        self.client.credentials(HTTP_AUTHORIZATION=TOKEN_AUTH + self.user.data["key"])

    def test_valid_get(self):
        """Test sending a valid get request to the serializer."""
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 200)
        self.assertIn("id", response.data[0])
        self.assertEqual(response.data[0]["username"], "testUser1")
        self.assertEqual(response.data[0]["displayName"], DISPLAY_NAME)
        self.assertEqual(
            response.data[0]["groups"],
            [OrderedDict([("id", 1), ("name", "admin"), ("isActive", True)])],
        )

    def test_no_group_get(self):
        """Test sending a invalid get request to the serializer."""
        non_admin_user = self.client.post(
            REGISTER_URL,
            {
                "username": "testUser2",
                "displayName": DISPLAY_NAME_2,
                "password1": "testPassword",
                "password2": "testPassword",
            },
        )
        self.client.credentials(
            HTTP_AUTHORIZATION=TOKEN_AUTH + non_admin_user.data["key"]
        )
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 403)

    def test_valid_update_as_admin(self):
        """Test sending a valid patch request to the serializer as admin."""
        user_id = self.user.data["user"]["id"]  # pylint: disable=E1101
        response = self.client.patch(
            f"{self.url}{user_id}/update/", {"displayName": DISPLAY_NAME_2},
        )
        self.assertEqual(response.status_code, 200)
        self.assertIn("id", response.data)
        self.assertEqual(response.data["username"], "testUser1")
        self.assertEqual(response.data["displayName"], DISPLAY_NAME_2)
        self.assertEqual(
            response.data["groups"],
            [OrderedDict([("id", 1), ("name", "admin"), ("isActive", True)])],
        )

    def test_valid_update_as_non_admin(self):
        """Test sending a valid patch request to the serializer as nonAdmin."""
        non_admin_user = self.client.post(
            REGISTER_URL,
            {
                "username": "testUser2",
                "displayName": DISPLAY_NAME_2,
                "password1": "testPassword",
                "password2": "testPassword",
            },
        )
        self.client.credentials(
            HTTP_AUTHORIZATION=TOKEN_AUTH + non_admin_user.data["key"]
        )
        user_id = non_admin_user.data["user"]["id"]
        response = self.client.patch(
            f"{self.url}{user_id}/update/", {"displayName": DISPLAY_NAME},
        )
        self.assertEqual(response.status_code, 200)
        self.assertIn("id", response.data)
        self.assertEqual(response.data["username"], "testUser2")
        self.assertEqual(response.data["displayName"], DISPLAY_NAME)

    def test_incorrect_user_update_as_non_admin(self):
        """Send put request where the user id is not the request user as nonAdmin."""
        non_admin_user = self.client.post(
            REGISTER_URL,
            {
                "username": "testUser2",
                "displayName": DISPLAY_NAME_2,
                "password1": "testPassword",
                "password2": "testPassword",
            },
        )
        self.client.credentials(
            HTTP_AUTHORIZATION=TOKEN_AUTH + non_admin_user.data["key"]
        )
        new_user = CustomUser.objects.create(username="testUser3")
        response = self.client.put(
            f"{self.url}{new_user.id}/update/", {"displayName": "test"},
        )
        self.assertEqual(response.status_code, 403)

    def test_other_user_update_as_admin(self):
        """Send patch request where the user id is not the request user as admin."""
        new_user = CustomUser.objects.create(username="testUser2")
        response = self.client.patch(
            f"{self.url}{new_user.id}/update/", {"displayName": "test"},
        )
        self.assertEqual(response.status_code, 200)
        self.assertIn("id", response.data)
        self.assertEqual(response.data["username"], "testUser2")
        self.assertEqual(response.data["displayName"], "test")

    def test_update_user_groups_as_admin(self):
        """Send patch request to update a users groups."""
        new_user = CustomUser.objects.create(username="testUser2")
        response = self.client.patch(
            f"{self.url}{new_user.id}/update/", {"groups": [1]},
        )
        self.assertEqual(response.status_code, 200)
        self.assertIn("id", response.data)
        self.assertEqual(response.data["username"], "testUser2")
        self.assertEqual(
            response.data["groups"],
            [OrderedDict([("id", 1), ("name", "admin"), ("isActive", True)])],
        )


class GroupSerializer(TestCase):
    """Tests for GroupSerializer."""

    url = "/api/groups/"
    user = None

    def setUp(self):
        """Create a user for auth."""
        self.client = APIClient()
        self.user = self.client.post(
            REGISTER_URL,
            {
                "username": "testUser1",
                "displayName": DISPLAY_NAME,
                "password1": "testPassword",
                "password2": "testPassword",
            },
        )
        self.client.credentials(HTTP_AUTHORIZATION=TOKEN_AUTH + self.user.data["key"])

    def test_valid_get_as_admin(self):
        """Test sending a valid get request to the serializer as admin."""
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.data), 1)
        self.assertEqual(
            response.data,
            [OrderedDict([("id", 1), ("name", "admin"), ("isActive", True)])],
        )

    def test_get_as_non_admin(self):
        """Test sending a invalid get request to the serializer as nonAdmin."""
        non_admin_user = self.client.post(
            REGISTER_URL,
            {
                "username": "testUser2",
                "displayName": DISPLAY_NAME_2,
                "password1": "testPassword",
                "password2": "testPassword",
            },
        )
        self.client.credentials(
            HTTP_AUTHORIZATION=TOKEN_AUTH + non_admin_user.data["key"]
        )
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 403)


class AdminPasswordChangeSerializer(TestCase):
    """Tests for AdminPasswordChangeSerializer."""

    url = "/api/change-password-admin/"
    admin_user = None
    non_admin_user = None

    def setUp(self):
        """Create a user for auth."""
        self.client = APIClient()
        self.admin_user = self.client.post(
            REGISTER_URL,
            {
                "username": "testUser1",
                "displayName": DISPLAY_NAME,
                "password1": "testPassword",
                "password2": "testPassword",
            },
        )
        self.client.credentials(
            HTTP_AUTHORIZATION=TOKEN_AUTH + self.admin_user.data["key"]
        )
        self.non_admin_user = self.client.post(
            REGISTER_URL,
            {
                "username": "testUser2",
                "displayName": DISPLAY_NAME,
                "password1": "testPassword",
                "password2": "testPassword",
            },
        )

    def test_valid_password_change(self):
        """An admin should be able to change a users password."""
        response = self.client.post(
            self.url,
            {
                "new_password1": "newPassword",
                "new_password2": "newPassword",
                "userId": self.non_admin_user.data["user"]["id"],
            },
        )
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data, {"detail": "New password has been saved."})
        non_admin_user_object = CustomUser.objects.get(
            id=self.non_admin_user.data["user"]["id"]
        )
        self.assertFalse(non_admin_user_object.check_password("testPassword"))
        self.assertTrue(non_admin_user_object.check_password("newPassword"))

    def test_invalid_user_id(self):
        """Should return validation message on incorrect user id."""
        response = self.client.post(
            self.url,
            {
                "new_password1": "newPassword",
                "new_password2": "newPassword",
                "userId": 999,
            },
        )
        self.assertEqual(response.status_code, 400)
        self.assertEqual(
            response.data,
            {
                "non_field_errors": [
                    ErrorDetail(string="This user does not exist", code="invalid")
                ]
            },
        )

    def test_non_matching_passwords(self):
        """Should return validation error when passwords don't match."""
        response = self.client.post(
            self.url,
            {
                "new_password1": "newPassword",
                "new_password2": "wrongPassword",
                "userId": self.non_admin_user.data["user"]["id"],
            },
        )
        self.assertEqual(response.status_code, 400)
        self.assertEqual(
            response.data,
            {
                "new_password2": [
                    ErrorDetail(
                        string="The two password fields didn’t match.", code="invalid"
                    )
                ]
            },
        )
