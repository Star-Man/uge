"""Test websocket consumers for user model."""
import asyncio

import pytest
from channels.db import database_sync_to_async
from channels.testing import WebsocketCommunicator
from rest_framework.authtoken.models import Token

from ...authentication.models import CustomGroup, CustomUser
from ...routing import APPLICATION

WEBSOCKET_URL = "ws/main/"


def create_default_groups():
    """Create the default groups since pytest doesn't apply migrations."""
    CustomGroup.objects.get_or_create(name="admin")
    CustomGroup.objects.get_or_create(name="nonAdmin")


@database_sync_to_async
def tokens():
    """Create users and return tokens."""
    create_default_groups()
    admin, _ = CustomUser.objects.get_or_create(username="testUser")
    admin_token, _ = Token.objects.get_or_create(user=admin)
    non_admin, _ = CustomUser.objects.get_or_create(username="testUser1")
    non_admin_token, _ = Token.objects.get_or_create(user=non_admin)
    return {
        "nonAdmin": non_admin_token,
        "admin": admin_token,
    }


@database_sync_to_async
def create_user(username):
    """Create a user model."""
    test_user = CustomUser.objects.create(username=username)
    test_user.save()


@database_sync_to_async
def update_non_admin():
    """Update the nonAdmin user."""
    non_admin_user = CustomUser.objects.get(username="testUser1")
    non_admin_user.displayName = "new name"
    return non_admin_user.save()


@database_sync_to_async
def update_groups():
    """Update the nonAdmin users group."""
    non_admin_user = CustomUser.objects.get(username="testUser1")
    admin = CustomGroup.objects.get(name="admin")
    return non_admin_user.groups.add(admin)


def check_user_data(response, expected):
    """Assert a users data is correct."""
    assert response["type"] == "update"
    assert "id" in response["data"]
    assert response["data"]["username"] == expected["username"]
    if "displayName" in expected:
        assert response["data"]["displayName"] == expected["displayName"]
    else:
        assert response["data"]["displayName"] == ""
    assert response["data"]["isActive"]


@pytest.mark.django_db(transaction=True)
class TestConsumers:
    """Test websocket consumers for user model."""

    @pytest.mark.asyncio
    async def test_consumer_updated_on_user_group_update(self):
        """Validate websocket message is sent when updating a users group."""
        user_tokens = await tokens()
        communicator = WebsocketCommunicator(
            APPLICATION, f'{WEBSOCKET_URL}?token={user_tokens["nonAdmin"]}'
        )
        connected, _ = await communicator.connect()
        assert connected
        await update_groups()
        response = await communicator.receive_json_from()
        assert response["data"]["groups"][0]["name"] == "admin"
        await communicator.disconnect()

    @pytest.mark.asyncio
    async def test_consumer_updated_on_save_user_model_admin(self):
        """Validate websocket message is sent to 'admin' on update to User."""
        user_tokens = await tokens()
        communicator = WebsocketCommunicator(
            APPLICATION, f'{WEBSOCKET_URL}?token={user_tokens["admin"]}'
        )
        connected, _ = await communicator.connect()
        assert connected
        await create_user("testUser5")
        response = await communicator.receive_json_from()
        check_user_data(
            response, {"username": "testUser5", "group": "nonAdmin"},
        )
        await communicator.disconnect()

    @pytest.mark.asyncio
    async def test_consumer_updated_on_save_user_model_self(self):
        """Validate websocket message is sent to own user on update to User."""
        user_tokens = await tokens()
        communicator = WebsocketCommunicator(
            APPLICATION, f'{WEBSOCKET_URL}?token={user_tokens["nonAdmin"]}'
        )
        connected, _ = await communicator.connect()
        assert connected
        await update_non_admin()
        response = await communicator.receive_json_from()
        check_user_data(
            response,
            {
                "username": "testUser1",
                "group": "nonAdminUser",
                "displayName": "new name",
            },
        )
        await communicator.disconnect()

    @pytest.mark.asyncio
    async def test_consumer_updated_on_save_user_model_non_admin_user(self):
        """Validate websocket message is sent to 'nonAdmin' on update to User."""
        user_tokens = await tokens()
        communicator = WebsocketCommunicator(
            APPLICATION, f'{WEBSOCKET_URL}?token={user_tokens["nonAdmin"]}'
        )
        connected, _ = await communicator.connect()
        assert connected
        await create_user("testUser6")
        response = await communicator.receive_json_from()
        check_user_data(
            response, {"username": "testUser6", "group": "nonAdmin"},
        )
        await communicator.disconnect()

    @pytest.mark.asyncio
    async def test_consumer_updated_on_save_user_model_external(self):
        """Validate websocket message is not sent to 'external' on update to User."""
        await database_sync_to_async(create_default_groups)()
        communicator = WebsocketCommunicator(APPLICATION, WEBSOCKET_URL)
        connected, _ = await communicator.connect()
        assert connected
        await create_user("testUser7")
        with pytest.raises(asyncio.TimeoutError):
            await communicator.receive_json_from(timeout=0.5)
