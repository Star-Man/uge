"""Form Tests."""
from django.test import TestCase

from ..forms import CustomUserCreationForm


class CustomUserCreationFormTestCase(TestCase):
    """Test the CustomUserCreationForm Form functionality."""

    def test_valid_form(self):
        """Test creating user correctly."""
        form_data = {
            "username": "testUser",
            "displayName": "Test User",
            "password1": "testPassword",
            "password2": "testPassword",
        }
        form = CustomUserCreationForm(data=form_data)
        self.assertTrue(form.is_valid())

    def test_non_matching_password(self):
        """Test creating user where passwords don't match."""
        form_data = {
            "username": "testUser",
            "displayName": "Test User",
            "password1": "testPassword",
            "password2": "tbestPassword",
        }
        form = CustomUserCreationForm(data=form_data)
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors, {"password2": ["The two password fields didn’t match."]}
        )

    def test_no_data(self):
        """Test creating user no data is provided."""
        form = CustomUserCreationForm(data={})
        self.assertFalse(form.is_valid())
        error = "This field is required."
        self.assertEqual(
            form.errors,
            {
                "username": [error],
                "displayName": [error],
                "password1": [error],
                "password2": [error],
            },
        )
