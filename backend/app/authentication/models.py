"""Django models are python object representations of database models."""

from __future__ import unicode_literals

from django.contrib.auth.models import AbstractUser
from django.db import models


class CustomGroup(models.Model):
    """Expand on the existing django group field."""

    isActive = models.BooleanField(default=True)
    name = models.CharField(verbose_name="Name", max_length=256)


class CustomUser(AbstractUser):
    """Expand on the existing django user field."""

    email = None
    displayName = models.CharField(verbose_name="Display Name", max_length=256)
    groups = models.ManyToManyField(CustomGroup)
