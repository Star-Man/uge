"""Override default user creation."""

from allauth.account.adapter import DefaultAccountAdapter


class CustomAccountAdapter(DefaultAccountAdapter):
    """Override default user creation to add extra fields."""

    def save_user(self, request, user, form, commit=False):
        """Override save_user function."""
        user = super().save_user(request, user, form, commit)
        data = form.cleaned_data
        user.displayName = data.get("displayName")
        user.save()
        return user
