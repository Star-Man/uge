"""Application configuration objects store metadata for an application."""
# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.apps import AppConfig


class AuthenticationAppConfig(AppConfig):
    """Application configuration for the authentication module."""

    name = "app.authentication"

    def ready(self):
        """Import signals module on load."""
        from . import signals  # pylint: disable=C0415 # noqa: W0611
