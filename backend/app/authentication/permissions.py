"""Declaration of serializer permissions."""
from rest_framework import permissions


class IsAdmin(permissions.BasePermission):
    """Check if the user is an admin."""

    def has_permission(self, request, view):
        """Check the permission."""
        if request.user and request.user.groups.filter(name="admin"):
            return True
        return False


class IsAnyGroup(permissions.BasePermission):
    """Check if the user has a group."""

    def has_permission(self, request, view):
        """Check the permission."""
        if request.user and request.user.groups.count() > 0:
            return True
        return False


class IsUserOrAdmin(permissions.BasePermission):
    """Custom permission to only allow user to edit their own user object."""

    def has_object_permission(self, request, view, obj):
        """Validate permissions for single object."""
        if request.user and request.user.groups.filter(name="admin"):
            return True
        return obj == request.user
