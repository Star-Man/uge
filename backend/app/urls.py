"""Root URLs routes."""

from django.conf.urls import include, url

urlpatterns = [  # pylint: disable=C0103
    url(r"api/", include("app.authentication.urls"), name="auth"),
]
