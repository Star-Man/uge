"""Django settings for the uge backend project."""

import os
from pathlib import Path

from environs import Env

ENV_PATH = str(
    Path(__file__).resolve().parent.parent.parent
    / "config"
    / f'{os.getenv("UGE_ENV", "local")}.env'
)
ENV = Env()
ENV.read_env(ENV_PATH)

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

SECRET_KEY = ENV("DJANGO_SECRET_KEY")

DEBUG = ENV.bool("DJANGO_DEBUG")
SESSION_COOKIE_SECURE = ENV.bool("SESSION_COOKIE_SECURE")
CSRF_COOKIE_SECURE = ENV.bool("CSRF_COOKIE_SECURE")
X_FRAME_OPTIONS = "DENY"
SECURE_HSTS_SECONDS = 3600
ALLOWED_HOSTS = [ENV("VUE_APP_API_HOST", "127.0.0.1"), "localhost"]
SECURE_HSTS_INCLUDE_SUBDOMAINS = True
SECURE_HSTS_PRELOAD = True

INSTALLED_APPS = [
    "django.contrib.admin",
    "django.contrib.auth",
    "django.contrib.sites",
    "django.contrib.contenttypes",
    "django.contrib.sessions",
    "django.contrib.messages",
    "django.contrib.staticfiles",
    "rest_framework",
    "rest_framework.authtoken",
    "rest_auth",
    "allauth",
    "allauth.account",
    "allauth.socialaccount",
    "rest_auth.registration",
    "corsheaders",
    "recurrence",
    "app.authentication.apps.AuthenticationAppConfig",
    "app.websockets",
    "channels",
]

MIDDLEWARE = [
    "corsheaders.middleware.CorsMiddleware",
    "django.middleware.security.SecurityMiddleware",
    "whitenoise.middleware.WhiteNoiseMiddleware",
    "django.contrib.sessions.middleware.SessionMiddleware",
    "django.middleware.common.CommonMiddleware",
    "django.middleware.csrf.CsrfViewMiddleware",
    "django.contrib.auth.middleware.AuthenticationMiddleware",
    "django.contrib.messages.middleware.MessageMiddleware",
    "django.middleware.clickjacking.XFrameOptionsMiddleware",
]

ROOT_URLCONF = "app.urls"

CORS_ORIGIN_ALLOW_ALL = True
CSRF_TRUSTED_ORIGINS = [
    f'{ENV("FRONTEND_INTEGRATION_TEST_HOST")}:{ENV("FRONTEND_PORT")}',
    f'127.0.0.1:{ENV("FRONTEND_PORT")}',
    f'localhost:{ENV("FRONTEND_PORT")}',
]
for frontend_host in ENV.list("FRONTEND_HOSTS"):
    CSRF_TRUSTED_ORIGINS.append(f'{frontend_host}:{ENV("FRONTEND_PORT")}')
    ALLOWED_HOSTS.append(frontend_host)


TEMPLATES = [
    {
        "BACKEND": "django.template.backends.django.DjangoTemplates",
        "DIRS": [],
        "APP_DIRS": True,
        "OPTIONS": {
            "context_processors": [
                "django.template.context_processors.debug",
                "django.template.context_processors.request",
                "django.contrib.auth.context_processors.auth",
                "django.contrib.messages.context_processors.messages",
            ]
        },
    }
]

WSGI_APPLICATION = "app.wsgi.application"

DATABASES = {
    "default": {
        "ENGINE": "django.db.backends.postgresql_psycopg2",
        "NAME": ENV("POSTGRES_DB"),
        "USER": ENV("POSTGRES_USER"),
        "PASSWORD": ENV("POSTGRES_PASSWORD"),
        "HOST": ENV("POSTGRES_HOST"),
        "PORT": ENV("POSTGRES_PORT"),
    }
}
LOGIN_REDIRECT_URL = "users"

LANGUAGE_CODE = "en-us"
TIME_ZONE = "Europe/Dublin"
USE_I18N = True
USE_L10N = True
USE_TZ = True

STATICFILES_DIRS = (os.path.join(BASE_DIR, "static"),)
STATIC_URL = "/static/"
MEDIA_URL = "/media/"
CHANNEL_LAYERS = {
    "default": {
        "BACKEND": "channels_redis.core.RedisChannelLayer",
        "CONFIG": {"hosts": [(ENV("REDIS_HOST"), ENV("REDIS_PORT"))]},
    }
}
LOGOUT_REDIRECT_URL = "/"

AUTHENTICATION_BACKENDS = ("django.contrib.auth.backends.ModelBackend",)

ACCOUNT_AUTHENTICATION_METHOD = "username"

ACCOUNT_EMAIL_REQUIRED = False
ACCOUNT_USERNAME_REQUIRED = True
ACCOUNT_LOGOUT_ON_GET = True
ACCOUNT_ADAPTER = "app.authentication.adapter.CustomAccountAdapter"
AUTH_USER_MODEL = "authentication.CustomUser"
AUTH_GROUP_MODEL = "authentication.CustomGroup"


REG_SERIALIZER = "app.authentication.serializers.CustomRegisterSerializer"
LOG_SERIALIZER = "app.authentication.serializers.CustomLoginSerializer"

REST_AUTH_REGISTER_SERIALIZERS = {"REGISTER_SERIALIZER": REG_SERIALIZER}
REST_AUTH_SERIALIZERS = {
    "LOGIN_SERIALIZER": LOG_SERIALIZER,
    "USER_DETAILS_SERIALIZER": "app.authentication.serializers.UserSerializer",
    "TOKEN_SERIALIZER": "app.authentication.serializers.TokenSerializer",
}
REST_FRAMEWORK = {
    "DEFAULT_AUTHENTICATION_CLASSES": [
        "rest_framework.authentication.TokenAuthentication"
    ],
    "DEFAULT_PERMISSION_CLASSES": [
        "rest_framework.permissions.DjangoModelPermissionsOrAnonReadOnly"
    ],
}
DJANGO_SITE = 1
ASGI_APPLICATION = "app.routing.APPLICATION"
LOGOUT_ON_PASSWORD_CHANGE = False
OLD_PASSWORD_FIELD_ENABLED = True
