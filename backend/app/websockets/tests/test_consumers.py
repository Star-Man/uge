"""Test consumers."""
import asyncio

import pytest
from channels.db import database_sync_to_async
from channels.testing import WebsocketCommunicator
from rest_framework.authtoken.models import Token

from ...authentication.models import CustomUser
from ...routing import APPLICATION

WEBSOCKET_URL = "ws/main/"


@pytest.mark.django_db()
@database_sync_to_async
def tokens():
    """Create users and return tokens."""
    admin, _ = CustomUser.objects.get_or_create(username="testUser")
    admin_token, _ = Token.objects.get_or_create(user=admin)
    non_admin, _ = CustomUser.objects.get_or_create(username="testUser1")
    non_admin_token, _ = Token.objects.get_or_create(user=non_admin)
    return {
        "nonAdmin": non_admin_token,
        "admin": admin_token,
    }


@pytest.mark.django_db()
class TestConsumers:
    """Test the websocket consumer."""

    @pytest.mark.asyncio
    async def test_consumer_valid_group_unauthenticated_external(self):
        """Consumer allowed for unauthenticated users when group is 'external'."""
        communicator = WebsocketCommunicator(APPLICATION, WEBSOCKET_URL)
        connected, _ = await communicator.connect()
        assert connected
        await communicator.send_json_to(
            {"type": "update", "message": "hello", "group": "external"}
        )
        response = await communicator.receive_json_from()
        assert response == {"message": "hello", "type": "update", "group": "external"}
        await communicator.disconnect()

    @pytest.mark.asyncio
    async def test_consumer_invalid_group_unauthenticated_non_admin(self):
        """Consumer denied for unauthenticated users when group is not 'external'."""
        communicator = WebsocketCommunicator(APPLICATION, WEBSOCKET_URL)
        connected, _ = await communicator.connect()
        assert connected
        await communicator.send_json_to(
            {"type": "update", "message": "hello", "group": "nonAdmin"}
        )
        with pytest.raises(asyncio.TimeoutError):
            await communicator.receive_json_from(timeout=0.5)

    @pytest.mark.asyncio
    async def test_consumer_valid_group_non_admin_external(self):
        """Consumer allowed for nonAdmin users when group is 'external'."""
        user_tokens = await tokens()
        communicator = WebsocketCommunicator(
            APPLICATION, f'{WEBSOCKET_URL}?token={user_tokens["nonAdmin"]}'
        )
        connected, _ = await communicator.connect()
        assert connected
        await communicator.send_json_to(
            {"type": "update", "message": "hello", "group": "external"}
        )
        response = await communicator.receive_json_from()
        assert response == {"message": "hello", "type": "update", "group": "external"}
        await communicator.disconnect()

    @pytest.mark.asyncio
    async def test_consumer_valid_group_non_admin_non_admin(self):
        """Consumer allowed for non_admin users when group is 'nonAdmin'."""
        user_tokens = await tokens()
        communicator = WebsocketCommunicator(
            APPLICATION, f'{WEBSOCKET_URL}?token={user_tokens["nonAdmin"]}'
        )
        connected, _ = await communicator.connect()
        assert connected
        await communicator.send_json_to(
            {"type": "update", "message": "hello", "group": "nonAdmin"}
        )
        response = await communicator.receive_json_from()
        assert response == {"message": "hello", "type": "update", "group": "nonAdmin"}
        await communicator.disconnect()

    @pytest.mark.asyncio
    async def test_consumer_invalid_group_non_admin_admin(self):
        """Consumer denied for non_admin users when group is 'admin'."""
        user_tokens = await tokens()
        communicator = WebsocketCommunicator(
            APPLICATION, f'{WEBSOCKET_URL}?token={user_tokens["nonAdmin"]}'
        )
        connected, _ = await communicator.connect()
        assert connected
        await communicator.send_json_to(
            {"type": "update", "message": "hello", "group": "admin"}
        )
        with pytest.raises(asyncio.TimeoutError):
            await communicator.receive_json_from(timeout=0.5)

    @pytest.mark.asyncio
    async def test_consumer_valid_group_admin_admin(self):
        """Consumer allowed for admin users when group is 'admin'."""
        user_tokens = await tokens()
        communicator = WebsocketCommunicator(
            APPLICATION, f'{WEBSOCKET_URL}?token={user_tokens["admin"]}'
        )
        connected, _ = await communicator.connect()
        assert connected
        await communicator.send_json_to(
            {"type": "update", "message": "hello", "group": "admin"}
        )
        response = await communicator.receive_json_from()
        assert response == {"message": "hello", "type": "update", "group": "admin"}
        await communicator.disconnect()

    @pytest.mark.asyncio
    async def test_consumer_valid_group_admin_non_admin(self):
        """Consumer allowed for admin users when group is 'nonAdmin'."""
        user_tokens = await tokens()
        communicator = WebsocketCommunicator(
            APPLICATION, f'{WEBSOCKET_URL}?token={user_tokens["admin"]}'
        )
        connected, _ = await communicator.connect()
        assert connected
        await communicator.send_json_to(
            {"type": "update", "message": "hello", "group": "nonAdmin"}
        )
        response = await communicator.receive_json_from()
        assert response == {"message": "hello", "type": "update", "group": "nonAdmin"}
        await communicator.disconnect()

    @pytest.mark.asyncio
    async def test_consumer_valid_group_admin_external(self):
        """Consumer allowed for admin users when group is 'external'."""
        user_tokens = await tokens()
        communicator = WebsocketCommunicator(
            APPLICATION, f'{WEBSOCKET_URL}?token={user_tokens["admin"]}'
        )
        connected, _ = await communicator.connect()
        assert connected
        await communicator.send_json_to(
            {"type": "update", "message": "hello", "group": "external"}
        )
        response = await communicator.receive_json_from()
        assert response == {"message": "hello", "type": "update", "group": "external"}
        await communicator.disconnect()

    @pytest.mark.asyncio
    async def test_consumer_headers_invalid(self):
        """Does not authenticate if 'token' is not passed as query name."""
        user_tokens = await tokens()
        communicator = WebsocketCommunicator(
            APPLICATION, f'{WEBSOCKET_URL}?TEST={user_tokens["admin"]}'
        )
        connected, _ = await communicator.connect()
        assert connected
        await communicator.send_json_to(
            {"type": "update", "message": "hello", "group": "non_admin"}
        )
        with pytest.raises(asyncio.TimeoutError):
            await communicator.receive_json_from(timeout=0.5)

    @pytest.mark.asyncio
    async def test_consumer_token_invalid_external(self):
        """Uses anonymous user that can access 'external' group if token is invalid."""
        communicator = WebsocketCommunicator(APPLICATION, f"{WEBSOCKET_URL}?token=TEST")
        connected, _ = await communicator.connect()
        assert connected
        await communicator.send_json_to(
            {"type": "update", "message": "hello", "group": "external"}
        )
        response = await communicator.receive_json_from()
        assert response == {"message": "hello", "type": "update", "group": "external"}
        await communicator.disconnect()

    @pytest.mark.asyncio
    async def test_consumer_token_invalid_non_admin(self):
        """Uses anonymous user that cannot access nonadmin group if token is invalid."""
        communicator = WebsocketCommunicator(APPLICATION, f"{WEBSOCKET_URL}?token=TEST")
        connected, _ = await communicator.connect()
        assert connected
        await communicator.send_json_to(
            {"type": "update", "message": "hello", "group": "non_admin"}
        )
        with pytest.raises(asyncio.TimeoutError):
            await communicator.receive_json_from(timeout=0.5)

    @pytest.mark.asyncio
    async def test_invalid_group_message(self):
        """Message does not send if group not provided."""
        user_tokens = await tokens()
        communicator = WebsocketCommunicator(
            APPLICATION, f'{WEBSOCKET_URL}?token={user_tokens["admin"]}'
        )
        connected, _ = await communicator.connect()
        assert connected
        await communicator.send_json_to({"type": "update", "message": "hello"})
        with pytest.raises(asyncio.TimeoutError):
            await communicator.receive_json_from(timeout=0.5)
