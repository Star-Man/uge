"""Authentication Middleware."""
import urllib.parse

from channels.auth import AuthMiddlewareStack
from channels.db import database_sync_to_async
from django.contrib.auth.models import AnonymousUser
from rest_framework.authtoken.models import Token


@database_sync_to_async
def get_user(token_key):
    """Get the current user model, or if none, return anonymous user."""
    try:
        token = Token.objects.get(key=token_key)
        return token.user
    except Token.DoesNotExist:
        return AnonymousUser()


class TokenAuthMiddlewareInstance:
    """Token authorization middleware for Django Channels 3."""

    def __init__(self, scope, middleware):
        """Init varaibles."""
        self.middleware = middleware
        self.scope = dict(scope)
        self.inner = self.middleware.inner

    async def __call__(self, receive, send):
        """Call instance."""
        decoded_qs = urllib.parse.parse_qs(self.scope["query_string"])
        if b"token" in decoded_qs:
            token = decoded_qs.get(b"token").pop().decode()
            self.scope["user"] = await get_user(token)
        return await self.inner(self.scope, receive, send)


class TokenAuthMiddleware:
    """Token authorization middleware wrapper."""

    def __init__(self, inner):
        """Init varaibles."""
        self.inner = inner

    def __call__(self, scope):
        """Call instance."""
        return TokenAuthMiddlewareInstance(scope, self)


def token_auth_middleware_stack(inner):
    """Token Auth Middleware Callable function."""
    return TokenAuthMiddleware(AuthMiddlewareStack(inner))
