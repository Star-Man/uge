"""Websocket consumers."""

from channels.db import database_sync_to_async
from channels.generic.websocket import AsyncJsonWebsocketConsumer


@database_sync_to_async
def check_user_group(user, group):
    """Return boolean of whether a user is in a group."""
    return user.groups.filter(name=group).exists()


class MainConsumer(AsyncJsonWebsocketConsumer):
    """Websocket consumer for authenticated connections."""

    user = None

    async def connect(self):
        """Run function on websocket connect."""
        self.user = self.scope["user"]
        await self.channel_layer.group_add("external", self.channel_name)

        if self.user.id is not None:
            await self.channel_layer.group_add("nonAdmin", self.channel_name)
            await self.channel_layer.group_add(str(self.user.id), self.channel_name)
            if await check_user_group(self.user, "admin"):
                await self.channel_layer.group_add("admin", self.channel_name)
        await self.accept()

    async def receive_json(self, content, **kwargs):
        """Run function on websocket recieve data."""
        if "group" in content:
            await self.channel_layer.group_send(content["group"], content)

    async def disconnect(self, code):
        """Disconnect from group on websocket disconnect."""
        await self.channel_layer.group_discard(str(self.user.id), self.channel_name)
        await self.channel_layer.group_discard("nonAdmin", self.channel_name)
        await self.channel_layer.group_discard("admin", self.channel_name)
        await self.channel_layer.group_discard("external", self.channel_name)

    async def update(self, event):
        """Recieve update messages for model."""
        await self.send_json(event)
