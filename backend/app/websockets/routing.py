"""Default app websocket routing."""
from channels.routing import ProtocolTypeRouter, URLRouter
from django.conf.urls import url
from django.core.asgi import get_asgi_application

from . import consumers
from .middleware import token_auth_middleware_stack

application = ProtocolTypeRouter(  # pylint: disable=C0103
    {
        "websocket": token_auth_middleware_stack(
            URLRouter([url(r"ws/main/$", consumers.MainConsumer.as_asgi())])
        ),
        "http": get_asgi_application(),
    }
)
