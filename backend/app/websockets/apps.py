"""Application configuration objects store metadata for an application."""
# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.apps import AppConfig


class AdministrationAppConfig(AppConfig):
    """Application configuration for the websockets module."""

    name = "app.websockets"
