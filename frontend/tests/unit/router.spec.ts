/* eslint-disable @typescript-eslint/no-explicit-any */
import VueRouter from 'vue-router';
import Vue from 'vue';
import { AxiosRequestConfig } from 'axios';
import router from '@/router';
import store from '@/store/store';

const handleErrors = (error: { type: number }) => {
	if (error.type !== 2) {
		throw error;
	}
};
describe('test router.ts', () => {
	let httpGet: jest.SpyInstance<
		Promise<unknown>,
		[string, (AxiosRequestConfig | undefined)?]
	>;
	let httpPatch: jest.SpyInstance<
		Promise<unknown>,
		[string, any?, (AxiosRequestConfig | undefined)?]
	>;
	beforeEach(() => {
		Vue.use(VueRouter);
		store.state.user = {
			id: 1,
			username: 'testUser',
			displayName: 'Test User',
			groups: [
				{ id: 4, name: 'admin', isActive: true },
				{ id: 4, name: 'group2', isActive: true }
			],
			isActive: true
		};
		store.state.token = 'test';
		httpGet = jest.spyOn(store.state.http, 'get');
		httpGet.mockResolvedValue({ data: [{ id: 4, setupComplete: true }] });
		httpPatch = jest.spyOn(store.state.http, 'patch');
		httpPatch.mockResolvedValue({});
	});

	it('admin can visit admin page', async () => {
		await router.replace('/admin').catch(err => handleErrors(err));
		expect(router.currentRoute.path).toStrictEqual('/admin');
	});

	it('unauthenticated users are always redirected to auth', async () => {
		store.state.token = null;
		await router.replace('/').catch(err => handleErrors(err));
		expect(router.currentRoute.path).toStrictEqual('/auth');
		await router.replace('/auth').catch(err => handleErrors(err));
		expect(router.currentRoute.path).toStrictEqual('/auth');
	});

	it('non admin user going to /admin is redirected to /user-settings', async () => {
		store.state.user.groups = [];
		await router.replace('/admin').catch(err => handleErrors(err));
		expect(router.currentRoute.path).toStrictEqual('/user-settings');
	});

	it('going to /auth when logged in redirects to /user-settings', async () => {
		await router.replace('/auth').catch(err => handleErrors(err));
		expect(router.currentRoute.path).toStrictEqual('/user-settings');
	});
});
