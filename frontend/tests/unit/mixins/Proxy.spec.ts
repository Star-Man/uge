/* eslint-disable @typescript-eslint/no-explicit-any */
import { createLocalVue, shallowMount } from '@vue/test-utils';
import Vuetify from 'vuetify';
import Vue from 'vue';
import VueRouter from 'vue-router';
import store from '@/store/store';
import User from '@/views/User.vue';

describe('test proxy mixin', () => {
	let wrapper: any;
	beforeEach(() => {
		Vue.use(Vuetify);
		const localVue = createLocalVue();
		localVue.use(Vuetify);
		localVue.use(VueRouter);
		const router = new VueRouter({
			routes: [
				{
					path: '/user-settings',
					name: 'userSettings',
					component: User
				}
			]
		});
		wrapper = shallowMount(User, {
			localVue,
			router,
			store
		});
		jest.clearAllMocks();
	});

	it('goToNextPage() routes to next url if passed', () => {
		const routerPush = jest.spyOn(wrapper.vm.$router, 'push');
		wrapper.vm.goToNextPage();
		expect(routerPush).toHaveBeenCalledTimes(1);
		expect(routerPush).toHaveBeenCalledWith({ name: 'userSettings' });
	});

	it('goToNextPage() routes to main if next url not passed', () => {
		const routerPush = jest.spyOn(wrapper.vm.$router, 'push');
		wrapper.vm.$route.query.nextUrl = '/test';
		wrapper.vm.goToNextPage();
		expect(routerPush).toHaveBeenCalledTimes(1);
		expect(routerPush).toHaveBeenCalledWith('/test');
	});

	it('goToNextPage() catches error when router.push fails to main', async () => {
		const routerPush = jest.spyOn(wrapper.vm.$router, 'push');
		routerPush.mockRejectedValueOnce('Example Error');
		wrapper.vm.goToNextPage();
		expect(routerPush).toHaveBeenCalledWith({ name: 'userSettings' });
	});

	it('goToNextPage() catches error when router.push fails to next', async () => {
		const routerPush = jest.spyOn(wrapper.vm.$router, 'push');
		wrapper.vm.$route.query.nextUrl = '/test';
		routerPush.mockRejectedValueOnce('Example Error');
		wrapper.vm.goToNextPage();
		expect(routerPush).toHaveBeenCalledWith('/test');
	});

	it('getUsers ignores users with no id', async () => {
		const httpGet = jest.spyOn(store.state.http, 'get');
		httpGet.mockResolvedValueOnce({
			data: [
				{
					username: 'test',
					displayName: 'Test User'
				}
			]
		});

		await wrapper.vm.getUsers();
		expect(wrapper.vm.$store.state.users).toStrictEqual({});
	});

	it('getUsers sets users group', async () => {
		const httpGet = jest.spyOn(store.state.http, 'get');
		httpGet.mockResolvedValueOnce({
			data: [
				{
					id: 2,
					username: 'test',
					displayName: 'Test User',
					groups: [{ id: 3, name: 'testGroup' }]
				}
			]
		});
		await wrapper.vm.getUsers();
		expect(wrapper.vm.$store.state.users).toStrictEqual({
			2: {
				id: 2,
				username: 'test',
				displayName: 'Test User',
				groups: [{ id: 3, name: 'testGroup' }]
			}
		});
	});

	it('getGroups ignores users with no id', async () => {
		const httpGet = jest.spyOn(store.state.http, 'get');
		httpGet.mockResolvedValueOnce({ data: [{ name: 'test' }] });
		await wrapper.vm.getGroups();
		expect(wrapper.vm.$store.state.groups).toStrictEqual({});
	});

	it('getGroups gets groups', async () => {
		const httpGet = jest.spyOn(store.state.http, 'get');
		httpGet.mockResolvedValueOnce({ data: [{ id: 3, name: 'test' }] });
		await wrapper.vm.getGroups();
		expect(wrapper.vm.$store.state.groups).toStrictEqual({
			'3': { id: 3, name: 'test' }
		});
	});

	it('getGroups calls axiosFailed on error', async () => {
		wrapper.vm.axiosFailed = jest.fn();
		const httpGet = jest.spyOn(store.state.http, 'get');
		httpGet.mockRejectedValueOnce(false);
		await wrapper.vm.getGroups();
		expect(wrapper.vm.axiosFailed).toHaveBeenCalledTimes(1);
	});
});
