/* eslint-disable @typescript-eslint/no-explicit-any */
import { createLocalVue, shallowMount } from '@vue/test-utils';
import Vuetify from 'vuetify';
import Vue from 'vue';
import VueRouter from 'vue-router';
import store from '@/store/store';
import Login from '@/components/Auth/Login.vue';
import Auth from '@/views/Auth.vue';
import User from '@/views/User.vue';
import websocketSetup from '@/backend/websockets';

websocketSetup();

describe('test user mixin', () => {
	let wrapper: any;
	const userData = {
		data: {
			key: '11111111',
			user: {
				username: 'testUser',
				id: 3,
				groups: [{ id: 3, name: 'admin' }]
			}
		}
	};
	beforeEach(() => {
		Vue.use(Vuetify);
		const localVue = createLocalVue();
		localVue.use(Vuetify);
		localVue.use(VueRouter);
		store.state.http.get = jest
			.fn()
			.mockImplementation(() => Promise.resolve({ data: [] }));
		const router = new VueRouter({
			routes: [
				{
					path: '/auth',
					name: 'auth',
					component: Auth
				},
				{
					path: '/user-settings',
					name: 'userSettings',
					component: User
				}
			]
		});

		wrapper = shallowMount(Login, {
			localVue,
			router,
			store
		});
		jest.clearAllMocks();
	});

	it('test loginSuccess function adds user to store', async () => {
		wrapper.vm.$router.push = () => Promise.resolve(true);
		const stateCommit = jest.spyOn(wrapper.vm.$store, 'commit');
		const routerPush = jest.spyOn(wrapper.vm.$router, 'push');

		routerPush.mockResolvedValueOnce(true);
		await wrapper.vm.loginSuccess(userData);
		expect(stateCommit).toHaveBeenCalledTimes(1);
		expect(stateCommit).toHaveBeenCalledWith('WS_CONNECT');
		expect(routerPush).toHaveBeenCalledTimes(1);
		expect(routerPush).toHaveBeenCalledWith({ name: 'userSettings' });
		expect(wrapper.vm.$store.state.token).toStrictEqual('11111111');
		expect(wrapper.vm.$store.state.user).toStrictEqual({
			username: 'testUser',
			id: 3,
			groups: [{ id: 3, name: 'admin' }]
		});
	});

	it('test loginSuccess catches error when router.push fails', async () => {
		const routerPush = jest.spyOn(wrapper.vm.$router, 'push');
		routerPush.mockRejectedValueOnce('Example Error');
		const stateCommit = jest.spyOn(wrapper.vm.$store, 'commit');
		await wrapper.vm.loginSuccess(userData);
		expect(stateCommit).toHaveBeenCalledWith('WS_CONNECT');
		expect(wrapper.vm.$store.state.token).toStrictEqual('11111111');
	});

	it('test logout deletes user data from store', async () => {
		wrapper.vm.$store.state.user = {
			username: 'testUser',
			id: 3,
			groups: [{ id: 3, name: 'admin' }],
			group: 'admin'
		};
		wrapper.vm.$store.state.token = 'test-token';
		await wrapper.vm.logout();
		expect(wrapper.vm.$store.state.token).toStrictEqual(null);
		expect(wrapper.vm.$store.state.user).toStrictEqual({
			username: null,
			id: null,
			groups: [],
			displayName: null,
			isActive: false
		});
	});
});
