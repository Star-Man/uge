import { createLocalVue, shallowMount } from '@vue/test-utils';
import Vuetify from 'vuetify';
import Vue, { VueConstructor } from 'vue';
import Snackbar from '@/components/Snackbar.vue';
import store from '@/store/store';

describe('Snackbar.vue', () => {
	// eslint-disable-next-line @typescript-eslint/no-explicit-any
	let wrapper: any;
	let localVue: VueConstructor<Vue>;

	beforeEach(() => {
		Vue.use(Vuetify);
		localVue = createLocalVue();
		localVue.use(Vuetify);
		wrapper = shallowMount(Snackbar, {
			localVue,
			store
		});
	});

	it('test created()', async () => {
		wrapper.vm.updateSnackbar = jest.fn();
		await wrapper.vm.$options.created[0].call(wrapper.vm);
		expect(wrapper.vm.updateSnackbar).toHaveBeenCalledTimes(0);
		wrapper.vm.$store.state.snackbar = {
			text: 'Example Error',
			color: 'error'
		};
		await wrapper.vm.$nextTick();
		expect(wrapper.vm.updateSnackbar).toHaveBeenCalledTimes(1);
		expect(wrapper.vm.updateSnackbar).toHaveBeenCalledWith(
			{
				text: 'Example Error',
				color: 'error'
			},
			{ color: '', text: '' }
		);
	});

	it('test buttonFunction()', async () => {
		wrapper.vm.updateSnackbar = jest.fn();
		const passedFunction = jest.fn();
		await wrapper.vm.$options.created[0].call(wrapper.vm);
		wrapper.vm.snackbarQueue = {
			1: {},
			2: {},
			3: {},
			4: {}
		};
		wrapper.vm.buttonFunction(passedFunction, 2);
		expect(passedFunction).toBeCalledTimes(1);
		expect(wrapper.vm.snackbarQueue).toStrictEqual({
			1: {},
			3: {},
			4: {}
		});
	});

	it('test updateSnackbar() with empty values', () => {
		expect(wrapper.vm.uniqueSnackbarId).toStrictEqual(0);
		expect(wrapper.vm.snackbarQueue).toStrictEqual({});
		const snackbar = {
			text: '',
			color: 'error'
		};
		wrapper.vm.updateSnackbar(snackbar);
		expect(wrapper.vm.uniqueSnackbarId).toStrictEqual(0);
		expect(wrapper.vm.snackbarQueue).toStrictEqual({});
	});

	it('test updateSnackbar() with values', () => {
		jest.useFakeTimers();
		expect(wrapper.vm.uniqueSnackbarId).toStrictEqual(0);
		expect(wrapper.vm.snackbarQueue).toStrictEqual({});
		const snackbar = {
			text: 'Example',
			color: 'error'
		};
		wrapper.vm.updateSnackbar(snackbar);
		expect(setTimeout).toHaveBeenCalledTimes(1);
		expect(setTimeout).toHaveBeenLastCalledWith(expect.any(Function), 5000);
		expect(wrapper.vm.uniqueSnackbarId).toStrictEqual(1);
		expect(wrapper.vm.snackbarQueue).toStrictEqual({
			'1': {
				text: 'Example',
				color: 'error',
				id: 1,
				isActive: true,
				buttonFunctionCalled: false
			}
		});
		jest.runAllTimers();
		expect(wrapper.vm.snackbarQueue).toStrictEqual({});
	});

	it('test updateSnackbar() with an afterCloseFunction', () => {
		jest.useFakeTimers();
		const snackbar = {
			text: 'Example',
			color: 'error',
			afterCloseFunction: () => true
		};
		const afterCloseFunction = jest.spyOn(snackbar, 'afterCloseFunction');

		wrapper.vm.updateSnackbar(snackbar);
		jest.runAllTimers();
		expect(afterCloseFunction).toHaveBeenCalledTimes(1);
	});

	it('test updateSnackbar() where snackbar has been removed from queue', () => {
		jest.useFakeTimers();
		const snackbar = {
			text: 'Example',
			color: 'error'
		};

		wrapper.vm.updateSnackbar(snackbar);
		wrapper.vm.snackbarQueue = {};
		jest.runAllTimers();
		expect(wrapper.vm.snackbarQueue).toStrictEqual({});
	});
});
