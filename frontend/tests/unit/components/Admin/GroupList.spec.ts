/* eslint-disable @typescript-eslint/no-explicit-any */
import { createLocalVue, shallowMount } from '@vue/test-utils';
import Vuetify from 'vuetify';
import Vue from 'vue';
import flushPromises from 'flush-promises';
import GroupList from '@/components/Admin/GroupList.vue';
import store from '@/store/store';

describe('GroupList.vue', () => {
	let wrapper: any;
	let httpPatch: jest.SpyInstance<any, unknown[]>;
	let axiosFailed: jest.SpyInstance<any, unknown[]>;
	beforeEach(() => {
		Vue.use(Vuetify);
		const localVue = createLocalVue();
		localVue.use(Vuetify);
		wrapper = shallowMount(GroupList, {
			localVue,
			store
		});
		httpPatch = jest.spyOn(wrapper.vm.$store.state.http, 'patch');
		axiosFailed = jest.spyOn(wrapper.vm, 'axiosFailed');

		httpPatch.mockReset();
	});

	it('deleteGroup() deletes group', () => {
		const setGroupActiveLocal = jest.spyOn(
			wrapper.vm,
			'setGroupActiveLocal'
		);
		const setGroupActiveServer = jest.spyOn(
			wrapper.vm,
			'setGroupActiveServer'
		);

		httpPatch.mockResolvedValue({ data: {} });
		const group = {
			name: 'TEST'
		};
		wrapper.vm.deleteGroup(group);
		expect(setGroupActiveLocal).toBeCalledWith({ name: 'TEST' }, false);
		expect(wrapper.vm.$store.state.snackbar).toMatchObject({
			text: 'TEST has been deleted',
			color: 'info',
			buttonText: 'Undo'
		});
		wrapper.vm.$store.state.snackbar.buttonFunction();
		expect(setGroupActiveLocal).toBeCalledWith({ name: 'TEST' }, true);
		wrapper.vm.$store.state.snackbar.afterCloseFunction();
		expect(setGroupActiveServer).toBeCalledWith({ name: 'TEST' }, false);
	});

	it('test setGroupActiveLocal() marks a group active', async () => {
		wrapper.vm.$store.state.groups['3'] = {
			isActive: false,
			id: 3
		};
		const group = {
			id: 3
		};
		wrapper.vm.setGroupActiveLocal(group, true);
		expect(wrapper.vm.$store.state.groups['3'].isActive).toStrictEqual(
			true
		);
	});

	it('test setGroupActiveServer()', async () => {
		expect(axiosFailed).not.toHaveBeenCalled();
		httpPatch.mockRejectedValueOnce({});
		const group = {
			id: 1,
			displayName: 'TEST'
		};
		await wrapper.vm.setGroupActiveServer(group, true);
		expect(axiosFailed).toHaveBeenCalledTimes(1);
		httpPatch.mockResolvedValueOnce({ data: {} });
		await wrapper.vm.setGroupActiveServer(group, true);
		expect(httpPatch).toHaveBeenCalledWith(
			'groups/1/update/',
			{ isActive: true },
			{ headers: { Authorization: 'Token null' } }
		);
	});

	it('gets groups on create', async () => {
		const httpGet = jest.spyOn(store.state.http, 'get');
		httpGet.mockResolvedValue({
			data: [
				{
					id: 3,
					name: 'test group',
					isActive: true
				},
				{
					id: 4,
					name: 'test group 2',
					isActive: false
				}
			]
		});
		store.state.groups = {
			'3': {
				id: 3,
				name: 'test group',
				isActive: true
			}
		};
		await wrapper.vm.$options.created[0].call(wrapper.vm);
		expect(httpGet).toHaveBeenCalledTimes(1);
		expect(httpGet).toHaveBeenCalledWith('groups/');
		await wrapper.vm.$nextTick();
		expect(wrapper.vm.groups).toStrictEqual([
			{ id: 3, name: 'test group', isActive: true }
		]);
		expect(axiosFailed).not.toHaveBeenCalled();
	});

	it('axiosFailed is called on create if server failure', async () => {
		const httpGet = jest.spyOn(store.state.http, 'get');
		httpGet.mockReset();
		httpGet.mockRejectedValue({});
		await wrapper.vm.$options.created[0].call(wrapper.vm);
		await flushPromises();
		expect(axiosFailed).toHaveBeenCalledTimes(1);
	});

	it('openGroupCreationDialog is called on create if server failure', async () => {
		wrapper.vm.newGroupName = 'test';
		wrapper.vm.createGroupDialog = false;
		wrapper.vm.openGroupCreationDialog();
		expect(wrapper.vm.newGroupName).toStrictEqual('');
		expect(wrapper.vm.createGroupDialog).toStrictEqual(true);
	});

	it('upsertGroup() sends patch when passed group has id', async () => {
		wrapper.vm.$store.state.user.group = 'admin';
		wrapper.vm.$store.state.token = 'AAAAAAAA';
		httpPatch.mockResolvedValueOnce({});
		await wrapper.vm.upsertGroup({
			id: 8,
			name: 'test name'
		});
		expect(axiosFailed).toHaveBeenCalledTimes(0);
		expect(httpPatch).toHaveBeenCalledWith(
			'groups/8/update/',
			{
				name: 'test name'
			},
			{ headers: { Authorization: 'Token AAAAAAAA' } }
		);
	});

	it('upsertGroup() sends post when passed group has no id', async () => {
		wrapper.vm.$store.state.user.group = 'admin';
		wrapper.vm.$store.state.token = 'AAAAAAAA';
		const httpPost = jest.spyOn(wrapper.vm.$store.state.http, 'post');
		httpPost.mockResolvedValueOnce({
			data: [{ id: 9, name: 'test' }]
		});
		await wrapper.vm.upsertGroup({ name: 'test name' });
		expect(axiosFailed).toHaveBeenCalledTimes(0);
		expect(httpPost).toHaveBeenCalledWith(
			'groups/',
			{ name: 'test name' },
			{ headers: { Authorization: 'Token AAAAAAAA' } }
		);
	});

	it('upsertGroup() calls axiosFailed() on failure with id', async () => {
		wrapper.vm.$store.state.user.group = 'admin';
		httpPatch.mockRejectedValueOnce({});
		await wrapper.vm.upsertGroup({
			id: 2,
			name: 'test name'
		});
		expect(axiosFailed).toHaveBeenCalledTimes(1);
	});

	it('upsertGroup() calls axiosFailed() on failure without id', async () => {
		wrapper.vm.$store.state.user.group = 'admin';
		const httpPost = jest.spyOn(wrapper.vm.$store.state.http, 'post');
		httpPost.mockRejectedValueOnce({});
		await wrapper.vm.upsertGroup({ name: 'test name' });
		expect(axiosFailed).toHaveBeenCalledTimes(1);
	});

	it('upsertGroup() does nothing if unique rule fails for creation', async () => {
		wrapper.vm.$store.state.groups = [
			{ name: 'Test Group 3', isActive: true }
		];
		const httpPost = jest.spyOn(wrapper.vm.$store.state.http, 'post');
		httpPost.mockRejectedValueOnce(true);
		httpPatch.mockRejectedValueOnce(true);
		await wrapper.vm.upsertGroup({ name: 'Test Group 3' });
		expect(axiosFailed).toHaveBeenCalledTimes(0);
		expect(httpPost).toHaveBeenCalledTimes(0);
		expect(httpPatch).toHaveBeenCalledTimes(0);
	});

	it('upsertGroup() does nothing if unique rule fails for editing', async () => {
		wrapper.vm.$store.state.groups = [
			{ name: 'Test Group 3', isActive: true }
		];
		const httpPost = jest.spyOn(wrapper.vm.$store.state.http, 'post');
		httpPost.mockRejectedValueOnce(true);
		httpPatch.mockRejectedValueOnce(true);
		wrapper.vm.groups.push({ id: 7, name: 'Test Group 3' });
		await wrapper.vm.upsertGroup({ id: 7, name: 'Test Group 3' });
		expect(axiosFailed).toHaveBeenCalledTimes(0);
		expect(httpPost).toHaveBeenCalledTimes(0);
		expect(httpPatch).toHaveBeenCalledTimes(0);
	});
});
