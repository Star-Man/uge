/* eslint-disable @typescript-eslint/no-explicit-any */
import { createLocalVue, shallowMount } from '@vue/test-utils';
import Vuetify from 'vuetify';
import Vue from 'vue';
import flushPromises from 'flush-promises';
import UserList from '@/components/Admin/UserList.vue';
import store from '@/store/store';

describe('UserList.vue', () => {
	let wrapper: any;
	let httpPatch: jest.SpyInstance<any, unknown[]>;
	let axiosFailed: jest.SpyInstance<any, unknown[]>;
	beforeEach(() => {
		Vue.use(Vuetify);
		const localVue = createLocalVue();
		localVue.use(Vuetify);
		wrapper = shallowMount(UserList, {
			localVue,
			store
		});
		httpPatch = jest.spyOn(wrapper.vm.$store.state.http, 'patch');
		axiosFailed = jest.spyOn(wrapper.vm, 'axiosFailed');

		httpPatch.mockReset();
	});

	it('closeDialog() closes dialog', () => {
		wrapper.vm.changePasswordDialog = true;
		wrapper.vm.closeDialog();
		expect(wrapper.vm.changePasswordDialog).toStrictEqual(false);
	});

	it('test axiosFailed()', () => {
		wrapper.vm.registerAbilityLoading = true;
		wrapper.vm.$store.state.snackbar = {
			text: '',
			color: ''
		};
		wrapper.vm.axiosFailed();
		expect(wrapper.vm.$store.state.snackbar).toStrictEqual({
			text: 'There was a problem connecting to the backend server',
			color: 'error'
		});
	});

	it('test isCurrentUser()', () => {
		wrapper.vm.$store.state.user.id = 2;
		let isCurrentUser = wrapper.vm.isCurrentUser(2);
		expect(isCurrentUser).toStrictEqual(true);
		isCurrentUser = wrapper.vm.isCurrentUser(5);
		expect(isCurrentUser).toStrictEqual(false);
	});

	it('test editUser()', () => {
		const user = {
			groups: [{ id: 2 }]
		};
		wrapper.vm.editUserDialog = false;
		wrapper.vm.editUser(user);
		expect(wrapper.vm.editUserDialog).toStrictEqual(true);
		expect(wrapper.vm.selectedUser).toStrictEqual({
			groups: [{ id: 2 }]
		});
	});

	it('test deleteUser()', () => {
		const setUserActiveLocal = jest.spyOn(wrapper.vm, 'setUserActiveLocal');
		const setUserActiveServer = jest.spyOn(
			wrapper.vm,
			'setUserActiveServer'
		);

		httpPatch.mockResolvedValue({ data: {} });
		const user = {
			displayName: 'TEST'
		};
		wrapper.vm.deleteUser(user);
		expect(setUserActiveLocal).toBeCalledWith(
			{ displayName: 'TEST' },
			false
		);
		expect(wrapper.vm.$store.state.snackbar).toMatchObject({
			text: 'TEST has been deleted',
			color: 'info',
			buttonText: 'Undo'
		});
		wrapper.vm.$store.state.snackbar.buttonFunction();
		expect(setUserActiveLocal).toBeCalledWith(
			{ displayName: 'TEST' },
			true
		);
		wrapper.vm.$store.state.snackbar.afterCloseFunction();
		expect(setUserActiveServer).toBeCalledWith(
			{ displayName: 'TEST' },
			false
		);
	});

	it('test setUserActiveLocal() marks a user active', async () => {
		wrapper.vm.$store.state.users['3'] = {
			isActive: false,
			id: 3
		};
		const user = {
			id: 3
		};
		wrapper.vm.setUserActiveLocal(user, true);
		expect(wrapper.vm.$store.state.users['3'].isActive).toStrictEqual(true);
	});

	it('test setUserActiveServer()', async () => {
		expect(axiosFailed).not.toHaveBeenCalled();
		httpPatch.mockRejectedValueOnce({});
		const user = {
			id: 1,
			displayName: 'TEST'
		};
		await wrapper.vm.setUserActiveServer(user, true);
		expect(axiosFailed).toHaveBeenCalledTimes(1);
		httpPatch.mockResolvedValueOnce({ data: {} });
		await wrapper.vm.setUserActiveServer(user, true);
		expect(httpPatch).toHaveBeenCalledWith(
			'users/1/update/',
			{ isActive: true },
			{ headers: { Authorization: 'Token null' } }
		);
	});

	it('test updateUserGroup()', async () => {
		const user = {
			id: 6,
			groups: [{ id: 2 }]
		};
		httpPatch.mockResolvedValueOnce({ data: {} });
		expect(httpPatch).not.toHaveBeenCalled();
		await wrapper.vm.updateUserGroup(user);
		expect(httpPatch).toHaveBeenCalledWith(
			'users/6/update/',
			{ groups: [2] },
			{ headers: { Authorization: 'Token null' } }
		);
		expect(axiosFailed).not.toHaveBeenCalled();
	});

	it('test updateUserGroup() when server error', async () => {
		const user = {
			id: 6,
			groups: [{ id: 2 }]
		};
		httpPatch.mockRejectedValueOnce({});
		expect(httpPatch).not.toHaveBeenCalled();
		await wrapper.vm.updateUserGroup(user);
		expect(httpPatch).toHaveBeenCalledWith(
			'users/6/update/',
			{ groups: [2] },
			{ headers: { Authorization: 'Token null' } }
		);
		expect(axiosFailed).toHaveBeenCalled();
	});

	it('gets users on create', async () => {
		const httpGet = jest.spyOn(store.state.http, 'get');
		httpGet.mockResolvedValue({
			data: [
				{
					id: 3,
					username: 'test',
					groups: [
						{ id: 1, name: 'admin' },
						{ id: 2, name: 'group2' },
						{ id: 3, name: 'group3' }
					]
				}
			]
		});
		store.state.users = {};
		store.state.groups = {
			'1': {
				id: 1,
				name: 'admin',
				isActive: true
			},
			'2': {
				id: 2,
				name: 'group2',
				isActive: false
			},
			'3': {
				id: 3,
				name: 'group3',
				isActive: true
			}
		};
		await wrapper.vm.$options.created[0].call(wrapper.vm);
		expect(httpGet).toHaveBeenCalledTimes(1);
		expect(httpGet).toHaveBeenCalledWith('users/');
		await wrapper.vm.$nextTick();
		expect(wrapper.vm.$store.state.users['3'].username).toStrictEqual(
			'test'
		);
		expect(wrapper.vm.$store.state.users['3'].id).toStrictEqual(3);
		expect(axiosFailed).not.toHaveBeenCalled();
	});

	it('axiosFailed is called on create if server failure', async () => {
		const httpGet = jest.spyOn(store.state.http, 'get');
		httpGet.mockReset();
		httpGet.mockRejectedValue({});
		await wrapper.vm.$options.created[0].call(wrapper.vm);
		await flushPromises();
		expect(axiosFailed).toHaveBeenCalledTimes(1);
	});
});
