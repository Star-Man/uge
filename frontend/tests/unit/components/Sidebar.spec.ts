import { createLocalVue, shallowMount } from '@vue/test-utils';
import Vuetify from 'vuetify';
import Vue from 'vue';
import VueRouter from 'vue-router';
import Sidebar from '@/components/Sidebar.vue';
import store from '@/store/store';
import Admin from '@/views/Admin.vue';
import User from '@/views/User.vue';

describe('Sidebar.vue', () => {
	// eslint-disable-next-line @typescript-eslint/no-explicit-any
	let wrapper: any;
	beforeEach(() => {
		Vue.use(Vuetify);
		const localVue = createLocalVue();
		localVue.use(VueRouter);
		const router = new VueRouter({
			routes: [
				{
					path: '/admin',
					name: 'admin',
					component: Admin
				},
				{
					path: '/user-settings',
					name: 'userSettings',
					component: User
				}
			]
		});
		localVue.use(Vuetify);
		store.state.user = {
			groups: [
				{
					id: 1,
					name: 'admin',
					isActive: true
				}
			],
			username: 'testUser',
			displayName: 'Test User',
			id: 2,
			isActive: true
		};
		const vuetify = new Vuetify({});
		wrapper = shallowMount(Sidebar, {
			localVue,
			router,
			store,
			vuetify
		});
		jest.clearAllMocks();
	});

	afterEach(() => {
		wrapper.destroy();
	});

	it('test default data values', () => {
		expect(wrapper.vm.drawer).toStrictEqual(false);
	});

	it('test userLetter()', () => {
		const letterT = wrapper.vm.userLetter();
		expect(letterT).toStrictEqual('T');
		wrapper.vm.$store.state.user.displayName = 'example';
		const letterE = wrapper.vm.userLetter();
		expect(letterE).toStrictEqual('E');
		wrapper.vm.$store.state.user.displayName = null;
		const blankLetter = wrapper.vm.userLetter();
		expect(blankLetter).toStrictEqual('');
	});

	it('test updateAdminPage() updates admin to different group', () => {
		expect(wrapper.vm.pages.length).toStrictEqual(1);
		wrapper.vm.updateAdminPage([]);
		expect(wrapper.vm.pages.length).toStrictEqual(0);
		expect(wrapper.vm.$store.state.snackbar).toStrictEqual({
			text: `An admin has changed your groups`,
			color: 'info'
		});
	});

	it('test updateAdminPage() redirects page if current is admin', () => {
		wrapper.vm.$router.push({ name: 'admin' }).catch((err: Error) => err);
		const routerPush = jest.spyOn(wrapper.vm.$router, 'push');
		routerPush.mockResolvedValueOnce(true);
		expect(wrapper.vm.$router.currentRoute.name).toStrictEqual('admin');
		wrapper.vm.updateAdminPage([]);
		expect(routerPush).toHaveBeenCalledWith({ name: 'userSettings' });
	});

	it('test updateAdminPage() handles router.push errors', () => {
		wrapper.vm.$router.push({ name: 'admin' }).catch((err: Error) => err);
		const routerPush = jest.spyOn(wrapper.vm.$router, 'push');
		routerPush.mockRejectedValueOnce('Example Error');
		wrapper.vm.updateAdminPage([]);
		expect(routerPush).toHaveBeenCalledWith({ name: 'userSettings' });
	});

	it('test updateAdminPage() does nothing when group same', () => {
		expect(wrapper.vm.pages.length).toStrictEqual(1);
		wrapper.vm.updateAdminPage([{ id: 1, name: 'admin' }]);
		wrapper.vm.updateAdminPage([]);
		wrapper.vm.updateAdminPage([{ id: 1, name: 'admin' }]);
		expect(wrapper.vm.pages.length).toStrictEqual(1);
	});

	it('test groups', () => {
		wrapper.vm.$store.state.user.groups = [
			{ id: 1, name: 'admin' },
			{ id: 2, name: 'group2' }
		];
		expect(wrapper.vm.groups).toStrictEqual([
			{ id: 1, name: 'admin' },
			{ id: 2, name: 'group2' }
		]);

		wrapper.vm.$store.state.user.groups = [{ id: 2, name: 'group2' }];
		expect(wrapper.vm.groups).toStrictEqual([{ id: 2, name: 'group2' }]);
	});
});

describe('Sidebar.vue created hook', () => {
	// eslint-disable-next-line @typescript-eslint/no-explicit-any
	let wrapper: any;
	const vuetify = new Vuetify({});
	let router: VueRouter;

	beforeEach(() => {
		Vue.use(Vuetify);
		router = new VueRouter({
			routes: [
				{
					path: '/admin',
					name: 'admin',
					component: Admin
				}
			]
		});
	});

	afterEach(() => {
		wrapper.destroy();
	});

	it('test created() where user is admin', () => {
		const localVue = createLocalVue();
		store.state.user = {
			groups: [
				{
					id: 1,
					name: 'admin',
					isActive: true
				}
			],
			username: 'testUser',
			displayName: 'Test User',
			id: 2,
			isActive: true
		};
		wrapper = shallowMount(Sidebar, {
			localVue,
			store,
			vuetify,
			router
		});
		expect(wrapper.vm.pages.length).toStrictEqual(1);
		expect(wrapper.vm.pages[0].title).toStrictEqual('Admin');
	});

	it('test created() where user is not admin', () => {
		store.state.user = {
			groups: [
				{
					id: 1,
					name: 'group1',
					isActive: true
				}
			],
			username: 'testUser',
			displayName: 'Test User',
			id: 2,
			isActive: true
		};
		const localVue = createLocalVue();
		wrapper = shallowMount(Sidebar, {
			localVue,
			vuetify,
			store,
			router
		});
		expect(wrapper.vm.pages.length).toStrictEqual(0);
	});
});
