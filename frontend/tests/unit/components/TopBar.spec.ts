import { createLocalVue, shallowMount } from '@vue/test-utils';
import Vuetify from 'vuetify';
import Vue from 'vue';
import TopBar from '@/components/TopBar.vue';
import store from '@/store/store';

describe('TopBar.vue', () => {
	// eslint-disable-next-line @typescript-eslint/no-explicit-any
	let wrapper: any;
	beforeEach(() => {
		Vue.use(Vuetify);
		const localVue = createLocalVue();
		localVue.use(Vuetify);
		wrapper = shallowMount(TopBar, {
			localVue,
			store
		});
	});

	it('topBar() returns store value', () => {
		wrapper.vm.$store.state.topBar = {
			text: 'Example Text',
			color: 'error',
			active: true
		};
		expect(wrapper.vm.topBar).toStrictEqual({
			text: 'Example Text',
			color: 'error',
			active: true
		});
	});
});
