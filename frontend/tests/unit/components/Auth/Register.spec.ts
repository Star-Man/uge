import { createLocalVue, shallowMount } from '@vue/test-utils';
import Vuetify from 'vuetify';
import VueRouter from 'vue-router';
import Vue from 'vue';
import Register from '@/components/Auth/Register.vue';
import Auth from '@/views/Auth.vue';
import store from '@/store/store';

describe('Register.vue', () => {
	// eslint-disable-next-line @typescript-eslint/no-explicit-any
	let wrapper: any;
	beforeEach(() => {
		Vue.use(Vuetify);
		const localVue = createLocalVue();
		localVue.use(VueRouter);
		const router = new VueRouter({
			routes: [
				{
					path: '/',
					name: 'auth',
					component: Auth
				}
			]
		});
		localVue.use(Vuetify);
		wrapper = shallowMount(Register, {
			localVue,
			router,
			store
		});
		wrapper.vm.$vuetify.theme = {
			dark: false,
			themes: {
				light: {
					primary: '#FFFFFF'
				}
			}
		};
		jest.clearAllMocks();
	});

	it('test passwordRules()', () => {
		wrapper.vm.$refs.registerForm.validate = () => false;
		expect(wrapper.vm.passwordRules).toStrictEqual([]);

		wrapper.vm.confirmPassword = 'test';
		expect(typeof wrapper.vm.passwordRules).toStrictEqual('object');
		expect(typeof wrapper.vm.passwordRules[0]).toStrictEqual('function');
		expect(wrapper.vm.passwordRules[0]()).toStrictEqual(
			'Values do not match'
		);
	});

	it('test updateUsernameErrors()', () => {
		expect(typeof wrapper.vm.updateUsernameErrors).toBe('function');
		wrapper.vm.userNameErrors = ['test1', 'test2'];
		expect(wrapper.vm.userNameErrors).toStrictEqual(['test1', 'test2']);
		wrapper.vm.updateUsernameErrors();
		expect(wrapper.vm.userNameErrors).toStrictEqual([]);
	});

	it('test updateDisplayNameErrors()', () => {
		expect(typeof wrapper.vm.updateDisplayNameErrors).toBe('function');
		wrapper.vm.displayNameErrors = ['test1', 'test2'];
		expect(wrapper.vm.displayNameErrors).toStrictEqual(['test1', 'test2']);
		wrapper.vm.updateDisplayNameErrors();
		expect(wrapper.vm.displayNameErrors).toStrictEqual([]);
	});

	it('test revalidateOnPasswordChange()', () => {
		wrapper.vm.$refs.registerForm.validate = () => false;
		const validateField = jest.spyOn(wrapper.vm, 'validateField');
		wrapper.vm.revalidateOnPasswordChange();
		expect(validateField).toHaveBeenCalledTimes(1);
	});

	it('test revalidateOnConfirmPasswordChange()', () => {
		wrapper.vm.$refs.registerForm.validate = () => false;
		const validateField = jest.spyOn(wrapper.vm, 'validateField');
		wrapper.vm.revalidateOnConfirmPasswordChange();
		expect(validateField).toHaveBeenCalledTimes(1);
	});

	it('test validate()', () => {
		const register = jest.spyOn(wrapper.vm, 'register');

		wrapper.vm.$refs.registerForm.validate = () => false;
		wrapper.vm.validate();
		expect(register).not.toHaveBeenCalled();

		wrapper.vm.$refs.registerForm.validate = () => true;
		wrapper.vm.validate();
		expect(register).toHaveBeenCalledTimes(1);
	});

	it('test register()', async () => {
		const consoleError = jest.spyOn(console, 'error');
		// eslint-disable-next-line @typescript-eslint/no-empty-function
		consoleError.mockImplementation(() => {});
		const loginSuccess = jest.spyOn(wrapper.vm, 'loginSuccess');
		const registerFailed = jest.spyOn(wrapper.vm, 'registerFailed');
		const httpPost = jest.spyOn(wrapper.vm.$store.state.http, 'post');
		httpPost.mockRejectedValueOnce('Example Error');
		wrapper.vm.username = 'testUser';
		wrapper.vm.displayName = 'Test User';
		wrapper.vm.password = 'testPass';

		await wrapper.vm.register();
		expect(httpPost).toHaveBeenCalledWith('registration/', {
			username: 'testUser',
			displayName: 'Test User',
			password1: 'testPass',
			password2: 'testPass'
		});
		expect(registerFailed).toHaveBeenCalledWith('Example Error');
		expect(loginSuccess).toHaveBeenCalledTimes(0);

		wrapper.vm.loginSuccess.mockResolvedValue();
		httpPost.mockResolvedValue({ data: {} });
		await wrapper.vm.register();
		expect(registerFailed).toHaveBeenCalledTimes(1);
		expect(loginSuccess).toHaveBeenCalledWith({ data: {} });

		consoleError.mockRestore();
	});

	it('test registerFailed()', () => {
		const err = {
			response: {
				data: {
					username: ['test username error'],
					password1: ['test password error']
				}
			}
		};
		wrapper.vm.$store.state.snackbar = {
			text: '',
			color: ''
		};
		wrapper.vm.registerFailed(err);
		expect(wrapper.vm.userNameErrors).toStrictEqual([
			'test username error'
		]);
		expect(wrapper.vm.passwordErrors).toStrictEqual([
			'test password error'
		]);
		expect(wrapper.vm.$store.state.snackbar).toStrictEqual({
			text: '',
			color: ''
		});
		const expectedError = 'non field error';

		const nonFieldErr = {
			response: {
				data: {
					non_field_errors: [expectedError]
				}
			}
		};
		wrapper.vm.registerFailed(nonFieldErr);
		expect(wrapper.vm.$store.state.snackbar).toStrictEqual({
			text: expectedError,
			color: 'error'
		});
		const noResponseErr = {};

		wrapper.vm.$store.state.snackbar = {
			text: '',
			color: ''
		};
		wrapper.vm.registerFailed(noResponseErr);
		expect(wrapper.vm.$store.state.snackbar).toStrictEqual({
			text: 'There was a problem connecting to the backend server',
			color: 'error'
		});
	});
});
