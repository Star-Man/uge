import { createLocalVue, shallowMount } from '@vue/test-utils';
import Vuetify from 'vuetify';
import Vue from 'vue';
import VueRouter from 'vue-router';
import Login from '@/components/Auth/Login.vue';
import Auth from '@/views/Auth.vue';
import store from '@/store/store';

describe('Login.vue', () => {
	// eslint-disable-next-line @typescript-eslint/no-explicit-any
	let wrapper: any;
	beforeEach(() => {
		Vue.use(Vuetify);
		const localVue = createLocalVue();
		localVue.use(Vuetify);
		localVue.use(VueRouter);
		const router = new VueRouter({
			routes: [
				{
					path: '/',
					name: 'auth',
					component: Auth
				}
			]
		});
		wrapper = shallowMount(Login, {
			localVue,
			router,
			store
		});

		jest.clearAllMocks();
	});
	it('test updateUsernameErrors()', () => {
		expect(typeof wrapper.vm.updateUsernameErrors).toBe('function');
		wrapper.vm.userNameErrors = ['test1', 'test2'];
		expect(wrapper.vm.userNameErrors).toStrictEqual(['test1', 'test2']);
		wrapper.vm.updateUsernameErrors();
		expect(wrapper.vm.userNameErrors).toStrictEqual([]);
	});
	it('test updatePasswordErrors()', () => {
		expect(typeof wrapper.vm.updatePasswordErrors).toBe('function');
		wrapper.vm.passwordErrors = ['test1', 'test2'];
		expect(wrapper.vm.passwordErrors).toStrictEqual(['test1', 'test2']);
		wrapper.vm.updatePasswordErrors();
		expect(wrapper.vm.passwordErrors).toStrictEqual([]);
	});
	it('test validate()', () => {
		const login = jest.spyOn(wrapper.vm, 'login');

		wrapper.vm.$refs.loginForm.validate = () => false;
		wrapper.vm.validate();
		expect(login).not.toHaveBeenCalled();

		wrapper.vm.$refs.loginForm.validate = () => true;
		wrapper.vm.validate();
		expect(login).toHaveBeenCalledTimes(1);
	});
	it('test loginFailed()', () => {
		const err = {
			response: {
				data: {
					username: ['test username error'],
					password: ['test password error']
				}
			}
		};

		wrapper.vm.loginFailed(err);
		expect(wrapper.vm.userNameErrors).toStrictEqual([
			'test username error'
		]);
		expect(wrapper.vm.passwordErrors).toStrictEqual([
			'test password error'
		]);
		expect(wrapper.vm.$store.state.snackbar).toStrictEqual({
			text: '',
			color: ''
		});
		const expectedError = 'non field error';
		const nonFieldErr = {
			response: {
				data: {
					non_field_errors: [expectedError]
				}
			}
		};
		wrapper.vm.loginFailed(nonFieldErr);
		expect(wrapper.vm.$store.state.snackbar).toStrictEqual({
			text: expectedError,
			color: 'error'
		});

		const noResponseErr = {};

		wrapper.vm.$store.state.snackbar = {
			text: '',
			color: ''
		};
		wrapper.vm.loginFailed(noResponseErr);
		expect(wrapper.vm.$store.state.snackbar).toStrictEqual({
			text: 'There was a problem connecting to the backend server',
			color: 'error'
		});
	});

	it('test login()', async () => {
		const loginSuccess = jest.spyOn(wrapper.vm, 'loginSuccess');
		const loginFailed = jest.spyOn(wrapper.vm, 'loginFailed');
		const httpPost = jest.spyOn(wrapper.vm.$store.state.http, 'post');
		httpPost.mockRejectedValueOnce({});
		wrapper.vm.username = 'testUser';
		wrapper.vm.password = 'testPass';
		await wrapper.vm.login();
		expect(httpPost).toHaveBeenCalledWith('login/', {
			password: 'testPass',
			username: 'testUser'
		});
		expect(loginFailed).toHaveBeenCalledTimes(1);
		expect(loginSuccess).toHaveBeenCalledTimes(0);

		wrapper.vm.loginSuccess.mockResolvedValue();
		httpPost.mockResolvedValueOnce({});
		await wrapper.vm.login();
		expect(loginFailed).toHaveBeenCalledTimes(1);
		expect(loginSuccess).toHaveBeenCalledWith({});
	});
});
