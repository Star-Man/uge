import { createLocalVue, shallowMount } from '@vue/test-utils';
import Vuetify from 'vuetify';
import Vue from 'vue';
import ChangePassword from '@/components/Auth/ChangePassword.vue';
import store from '@/store/store';

describe('ChangePassword.vue', () => {
	// eslint-disable-next-line @typescript-eslint/no-explicit-any
	let wrapper: any;
	beforeEach(() => {
		Vue.use(Vuetify);
		const localVue = createLocalVue();
		localVue.use(Vuetify);
		wrapper = shallowMount(ChangePassword, {
			localVue,
			store
		});
		jest.clearAllMocks();
	});

	it('test revalidateOnOldPasswordChange() revalidates', () => {
		wrapper.vm.$refs.passwordChangeForm.validate = () => false;
		const validateField = jest.spyOn(wrapper.vm, 'validateField');
		wrapper.vm.revalidateOnOldPasswordChange();
		expect(validateField).toHaveBeenCalledTimes(1);
	});

	it('test revalidateOnPasswordChange() revalidates', () => {
		wrapper.vm.$refs.passwordChangeForm.validate = () => false;
		const validateField = jest.spyOn(wrapper.vm, 'validateField');
		wrapper.vm.revalidateOnPasswordChange();
		expect(validateField).toHaveBeenCalledTimes(1);
	});

	it('test revalidateOnConfirmPasswordChange() revalidates', () => {
		wrapper.vm.$refs.passwordChangeForm.validate = () => false;
		const validateField = jest.spyOn(wrapper.vm, 'validateField');
		wrapper.vm.revalidateOnConfirmPasswordChange();
		expect(validateField).toHaveBeenCalledTimes(1);
	});

	it('test validateField() clears existing errors and revalidates', () => {
		wrapper.vm.newPasswordErrors = [1, 2, 3];
		wrapper.vm.oldPasswordErrors = [4, 5, 6];
		wrapper.vm.$refs.passwordChangeForm.validate = () => false;
		const validate = jest.spyOn(
			wrapper.vm.$refs.passwordChangeForm,
			'validate'
		);
		wrapper.vm.validateField();
		expect(wrapper.vm.newPasswordErrors).toStrictEqual([]);
		expect(wrapper.vm.oldPasswordErrors).toStrictEqual([]);

		expect(validate).toHaveBeenCalledTimes(1);
	});

	it('test passwordRules()', () => {
		wrapper.vm.$refs.passwordChangeForm.validate = () => false;
		expect(wrapper.vm.passwordRules).toStrictEqual([]);

		wrapper.vm.confirmPassword = 'test';
		expect(typeof wrapper.vm.passwordRules).toStrictEqual('object');
		expect(typeof wrapper.vm.passwordRules[0]).toStrictEqual('function');
		expect(wrapper.vm.passwordRules[0]()).toStrictEqual(
			'Values do not match'
		);
	});

	it('test changePassword() while not an admin', async () => {
		const consoleError = jest.spyOn(console, 'error');
		// eslint-disable-next-line @typescript-eslint/no-empty-function
		consoleError.mockImplementation(() => {});
		const changePasswordSuccess = jest.spyOn(
			wrapper.vm,
			'changePasswordSuccess'
		);
		const changePasswordFailed = jest.spyOn(
			wrapper.vm,
			'changePasswordFailed'
		);
		const httpPost = jest.spyOn(wrapper.vm.$store.state.http, 'post');
		wrapper.vm.oldPassword = 'oldPassword';
		wrapper.vm.newPassword = 'newPassword';
		wrapper.vm.$store.state.token = 'test-token';
		httpPost.mockRejectedValueOnce('Example Error');
		await wrapper.vm.changePassword();
		expect(httpPost).toHaveBeenCalledWith(
			'change-password/',
			{
				new_password1: 'newPassword',
				new_password2: 'newPassword',
				old_password: 'oldPassword'
			},
			{ headers: { Authorization: 'Token test-token' } }
		);
		expect(changePasswordFailed).toHaveBeenCalledWith('Example Error');
		expect(changePasswordSuccess).toHaveBeenCalledTimes(0);

		wrapper.vm.changePasswordSuccess.mockResolvedValue();
		httpPost.mockResolvedValueOnce({ data: {} });
		await wrapper.vm.changePassword();
		expect(changePasswordFailed).toHaveBeenCalledTimes(1);
		expect(changePasswordSuccess).toHaveBeenCalledTimes(1);
		consoleError.mockRestore();
	});

	it('test changePasswordSuccess() resets validation', () => {
		wrapper.vm.$refs.passwordChangeForm.validate = () => true;
		wrapper.vm.$refs.passwordChangeForm.resetValidation = () => true;
		const resetValidation = jest.spyOn(
			wrapper.vm.$refs.passwordChangeForm,
			'resetValidation'
		);
		wrapper.vm.oldPassword = 'test';
		wrapper.vm.newPassword = 'test';
		wrapper.vm.confirmPassword = 'test';
		wrapper.vm.changePasswordSuccess();
		expect(resetValidation).toHaveBeenCalledTimes(1);
		expect(wrapper.vm.oldPassword).toStrictEqual('');
		expect(wrapper.vm.newPassword).toStrictEqual('');
		expect(wrapper.vm.confirmPassword).toStrictEqual('');
	});

	it('test changePasswordSuccess() calls snackbar', () => {
		wrapper.vm.$refs.passwordChangeForm.validate = () => true;
		wrapper.vm.$refs.passwordChangeForm.resetValidation = () => true;
		wrapper.vm.changePasswordSuccess();
		expect(wrapper.vm.$store.state.snackbar).toStrictEqual({
			text: 'Password changed!',
			color: 'success'
		});
	});

	it('test changePasswordSuccess() closes dialog', () => {
		wrapper.vm.$refs.passwordChangeForm.validate = () => true;
		wrapper.vm.$refs.passwordChangeForm.resetValidation = () => true;
		const emit = jest.spyOn(wrapper.vm, '$emit');
		wrapper.vm.changePasswordSuccess();
		expect(emit).toHaveBeenCalledWith('close-dialog');
	});

	it('test changePasswordFailed() with field errors', () => {
		const err = {
			response: {
				data: {
					old_password: ['old password error'],
					new_password: ['new password error']
				}
			}
		};
		wrapper.vm.changePasswordFailed(err);
		expect(wrapper.vm.newPasswordErrors).toStrictEqual([
			'new password error'
		]);
		expect(wrapper.vm.oldPasswordErrors).toStrictEqual([
			'old password error'
		]);
	});

	it('test changePasswordFailed() with non field errors', () => {
		const expectedError = 'non field error';

		const nonFieldErr = {
			response: {
				data: {
					non_field_errors: [expectedError]
				}
			}
		};
		wrapper.vm.changePasswordFailed(nonFieldErr);
		expect(wrapper.vm.$store.state.snackbar).toStrictEqual({
			text: expectedError,
			color: 'error'
		});
	});

	it('test changePasswordFailed() with no response error', () => {
		const noResponseErr = {};

		wrapper.vm.$store.state.snackbar = {
			text: '',
			color: ''
		};
		wrapper.vm.changePasswordFailed(noResponseErr);
		expect(wrapper.vm.$store.state.snackbar).toStrictEqual({
			text: 'There was a problem connecting to the backend server',
			color: 'error'
		});
	});
});

describe('ChangePassword.vue as admin', () => {
	// eslint-disable-next-line @typescript-eslint/no-explicit-any
	let wrapper: any;
	beforeEach(() => {
		Vue.use(Vuetify);
		const localVue = createLocalVue();
		localVue.use(Vuetify);
		wrapper = shallowMount(ChangePassword, {
			localVue,
			store,
			propsData: {
				userId: 5,
				admin: true
			}
		});
		jest.clearAllMocks();
	});

	it('test changePassword() while an admin', async () => {
		const consoleError = jest.spyOn(console, 'error');
		// eslint-disable-next-line @typescript-eslint/no-empty-function
		consoleError.mockImplementation(() => {});
		const changePasswordSuccess = jest.spyOn(
			wrapper.vm,
			'changePasswordSuccess'
		);
		const changePasswordFailed = jest.spyOn(
			wrapper.vm,
			'changePasswordFailed'
		);
		const httpPost = jest.spyOn(wrapper.vm.$store.state.http, 'post');
		wrapper.vm.oldPassword = 'oldPassword';
		wrapper.vm.newPassword = 'newPassword';
		wrapper.vm.$store.state.token = 'test-token';
		httpPost.mockRejectedValueOnce('Example Error');
		await wrapper.vm.changePassword();
		expect(httpPost).toHaveBeenCalledWith(
			'change-password-admin/',
			{
				userId: 5,
				new_password1: 'newPassword',
				new_password2: 'newPassword'
			},
			{ headers: { Authorization: 'Token test-token' } }
		);
		expect(changePasswordFailed).toHaveBeenCalledWith('Example Error');
		expect(changePasswordSuccess).toHaveBeenCalledTimes(0);

		wrapper.vm.changePasswordSuccess.mockResolvedValue();
		httpPost.mockResolvedValueOnce({ data: {} });
		await wrapper.vm.changePassword();
		expect(changePasswordFailed).toHaveBeenCalledTimes(1);
		expect(changePasswordSuccess).toHaveBeenCalledTimes(1);
		consoleError.mockRestore();
	});
});
