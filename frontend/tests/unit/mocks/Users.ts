import store from '@/store/store';

export default (): void => {
	store.state.users = {
		1: {
			id: 1,
			username: 'testUser',
			displayName: 'Test User',
			groups: [{ id: 4, name: 'admin', isActive: true }],
			isActive: true
		},
		2: {
			id: 2,
			username: 'testUser2',
			displayName: 'Test User 2',
			groups: [{ id: 3, name: 'group2', isActive: true }],
			isActive: true
		},
		3: {
			id: 3,
			username: 'testUser3',
			displayName: 'Test User 3',
			groups: [{ id: 3, name: 'group3', isActive: true }],
			isActive: true
		}
	};
};
