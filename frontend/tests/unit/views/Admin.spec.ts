/* eslint-disable @typescript-eslint/no-explicit-any */
import { createLocalVue, shallowMount } from '@vue/test-utils';
import Vuetify from 'vuetify';
import Vue from 'vue';
import Admin from '@/views/Admin.vue';
import store from '@/store/store';

describe('Admin.vue', () => {
	let wrapper: any;
	beforeEach(() => {
		Vue.use(Vuetify);
		const localVue = createLocalVue();
		localVue.use(Vuetify);
		wrapper = shallowMount(Admin, {
			localVue,
			store
		});
	});

	it('test components loaded', () => {
		expect(wrapper.vm.$options.components).toHaveProperty('user-list');
	});
});
