/* eslint-disable @typescript-eslint/no-explicit-any */
import { createLocalVue, shallowMount } from '@vue/test-utils';
import Vuetify from 'vuetify';
import Vue, { VueConstructor } from 'vue';
import Auth from '@/views/Auth.vue';
import store from '@/store/store';

describe('Auth.vue', () => {
	let localVue: VueConstructor<Vue>;
	let wrapper: any;

	beforeEach(() => {
		Vue.use(Vuetify);
		localVue = createLocalVue();
		localVue.use(Vuetify);
		jest.clearAllMocks();

		wrapper = shallowMount(Auth, {
			localVue,
			store
		});
	});

	it('test components loaded', () => {
		expect(wrapper.vm.$options.components).toHaveProperty('login');
		expect(wrapper.vm.$options.components).toHaveProperty('register');
	});

	it('test axiosFailed()', () => {
		wrapper.vm.axiosFailed();
		expect(wrapper.vm.$store.state.snackbar).toStrictEqual({
			text: 'There was a problem connecting to the backend server',
			color: 'error'
		});
	});
});
