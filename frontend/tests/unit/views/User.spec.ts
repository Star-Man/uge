/* eslint-disable @typescript-eslint/no-explicit-any */
import { createLocalVue, shallowMount } from '@vue/test-utils';
import Vuetify from 'vuetify';
import Vue from 'vue';
import User from '@/views/User.vue';
import store from '@/store/store';

describe('User.vue', () => {
	let wrapper: any;
	let httpPatch: jest.SpyInstance<any, unknown[]>;
	let axiosFailed: jest.SpyInstance<any, unknown[]>;

	beforeEach(() => {
		Vue.use(Vuetify);
		const localVue = createLocalVue();
		localVue.use(Vuetify);
		wrapper = shallowMount(User, {
			localVue,
			store
		});
		wrapper.vm.$store.state.user = {
			id: 1,
			username: 'testUser',
			displayName: 'Test User',
			group: 'nurse',
			groups: [{ id: 4, name: 'nurse' }],
			isActive: true
		};
		wrapper.vm.$store.state.token = 'TEST-TOKEN';
		httpPatch = jest.spyOn(wrapper.vm.$store.state.http, 'patch');
		axiosFailed = jest.spyOn(wrapper.vm, 'axiosFailed');

		jest.clearAllMocks();
	});

	it('validateDisplayName() calls updateUserSettings() when valid', () => {
		const updateDisplayName = jest.spyOn(wrapper.vm, 'updateDisplayName');
		wrapper.vm.$refs.displayNameForm.validate = () => false;
		wrapper.vm.validateDisplayName();
		expect(updateDisplayName).not.toHaveBeenCalled();
	});

	it("validateDisplayName() doesn't call updateUserSettings() when invalid", () => {
		const updateDisplayName = jest.spyOn(wrapper.vm, 'updateDisplayName');
		wrapper.vm.$refs.displayNameForm.validate = () => true;
		wrapper.vm.validateDisplayName();
		expect(updateDisplayName).toHaveBeenCalledTimes(1);
	});

	it('closeDialog() closes dialog', () => {
		wrapper.vm.changePasswordDialog = true;
		wrapper.vm.closeDialog();
		expect(wrapper.vm.changePasswordDialog).toStrictEqual(false);
	});

	it('axiosFailed() updates snackbar', () => {
		wrapper.vm.axiosFailed();
		expect(wrapper.vm.$store.state.snackbar).toStrictEqual({
			text: 'There was a problem connecting to the backend server',
			color: 'error'
		});
	});

	it('updating display name sends http patch request to server', async () => {
		httpPatch.mockResolvedValueOnce({ data: {} });
		expect(httpPatch).not.toHaveBeenCalled();
		await wrapper.vm.updateDisplayName();
		expect(httpPatch).toHaveBeenCalledWith(
			'users/1/update/',
			{ displayName: 'Test User' },
			{ headers: { Authorization: 'Token TEST-TOKEN' } }
		);
		expect(axiosFailed).not.toHaveBeenCalled();
	});

	it('updating display name handles error on server fail', async () => {
		httpPatch.mockRejectedValueOnce({});
		expect(httpPatch).not.toHaveBeenCalled();
		await wrapper.vm.updateDisplayName();
		expect(axiosFailed).toHaveBeenCalledTimes(1);
	});

	it('test beforeDestroy() stops watch functions', () => {
		const unwatchGetStateUserDisplayName = jest.spyOn(
			wrapper.vm,
			'unwatchGetStateUserDisplayName'
		);
		expect(unwatchGetStateUserDisplayName).toHaveBeenCalledTimes(0);
		wrapper.destroy();
		expect(unwatchGetStateUserDisplayName).toHaveBeenCalledTimes(1);
	});

	it('test unwatchGetStateUserDisplayName() is functional', () => {
		expect(wrapper.vm.unwatchGetStateUserDisplayName.name).toStrictEqual(
			'unwatchFn'
		);
	});

	it('test updateDisplayNameFromStore() updates display name', () => {
		expect(wrapper.vm.displayName).toStrictEqual('Test User');
		wrapper.vm.updateDisplayNameFromStore('test');
		expect(wrapper.vm.displayName).toStrictEqual('test');
	});

	it('test updateDisplayNameFromStore() does not set display name to blank', () => {
		expect(wrapper.vm.displayName).toStrictEqual('Test User');
		wrapper.vm.updateDisplayNameFromStore('');
		expect(wrapper.vm.displayName).toStrictEqual('Test User');
	});

	it('test getStateUserDisplayName() returns state display name', () => {
		const displayName = wrapper.vm.getStateUserDisplayName(
			wrapper.vm.$store.state
		);
		expect(displayName).toStrictEqual('Test User');
	});
});
