import { createLocalVue, shallowMount } from '@vue/test-utils';
import Vuetify from 'vuetify';
import Vue from 'vue';
import VueRouter from 'vue-router';
import DefaultLayout from '@/layouts/DefaultLayout.vue';

describe('DefaultLayout.vue', () => {
	// eslint-disable-next-line @typescript-eslint/no-explicit-any
	let wrapper: any;
	beforeEach(() => {
		Vue.use(Vuetify);
		const localVue = createLocalVue();
		localVue.use(Vuetify);
		localVue.use(VueRouter);
		wrapper = shallowMount(DefaultLayout, {
			localVue
		});
	});
	it('test components loaded', () => {
		expect(typeof wrapper.vm.$options.components.sidebar).toBe('object');
	});
});
