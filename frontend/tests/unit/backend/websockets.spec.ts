import Vue from 'vue';
import websocketSetup from '@/backend/websockets';

describe('test websockets.ts', () => {
	it('test components loaded', () => {
		expect(Vue.prototype).not.toHaveProperty('$connect');
		websocketSetup();
		expect(Vue.prototype).toHaveProperty('$connect');
	});
});
