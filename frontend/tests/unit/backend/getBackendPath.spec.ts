import getBackendPath from '@/backend/getBackendPath';

describe('getBackendPath.ts', () => {
	it('gets backend path with env set', () => {
		expect(getBackendPath()).toStrictEqual(
			`${process.env.VUE_APP_API_HOST}:${process.env.VUE_APP_API_PORT}`
		);
	});

	it('gets backend path without env set', () => {
		process.env.VUE_APP_API_HOST = '';
		expect(getBackendPath()).toStrictEqual(
			`${window.location.hostname}:${process.env.VUE_APP_API_PORT}`
		);
	});
});
