import { createLocalVue, shallowMount } from '@vue/test-utils';
import Vuetify from 'vuetify';
import Vue from 'vue';
import VueRouter from 'vue-router';
import App from '@/App.vue';
import store from '@/store/store';
import websocketSetup from '@/backend/websockets';

describe('App.vue', () => {
	// eslint-disable-next-line @typescript-eslint/no-explicit-any
	let wrapper: any;
	beforeEach(() => {
		Vue.use(Vuetify);
		websocketSetup();
		const router = new VueRouter({
			routes: []
		});
		store.state.user = {
			id: 1,
			username: 'testUser',
			displayName: 'Test User',
			groups: [{ id: 4, name: 'admin', isActive: true }],
			isActive: true
		};
		const localVue = createLocalVue();
		localVue.use(VueRouter);
		localVue.use(Vuetify);
		const vuetify = new Vuetify({});
		wrapper = shallowMount(App, {
			localVue,
			vuetify,
			router,
			store
		});
	});
	it('test components loaded', () => {
		expect(typeof wrapper.vm.$options.components.snackbar).toBe('object');
	});

	it('test isActive() function returns store value', () => {
		wrapper.vm.$store.state.user.isActive = true;
		expect(wrapper.vm.isActive).toStrictEqual(true);
	});

	it('updateUserActive() does not logout if no user id in store', () => {
		wrapper.vm.$store.state.user.id = null;
		const logout = jest.spyOn(wrapper.vm, 'logout');
		logout.mockResolvedValue({});
		wrapper.vm.updateUserActive(false);
		expect(logout).toHaveBeenCalledTimes(0);
	});

	it('updateUserActive() does not logout if isActive is true', () => {
		wrapper.vm.$store.state.user.id = 4;
		const logout = jest.spyOn(wrapper.vm, 'logout');
		logout.mockResolvedValue({});
		wrapper.vm.updateUserActive(true);
		expect(logout).toHaveBeenCalledTimes(0);
	});

	it('updateUserActive() calls logout if isActive is false and id exists', () => {
		wrapper.vm.$store.state.user.id = 4;
		const logout = jest.spyOn(wrapper.vm, 'logout');
		logout.mockResolvedValue({});
		wrapper.vm.updateUserActive(false);
		expect(logout).toHaveBeenCalledTimes(1);
		expect(wrapper.vm.$store.state.snackbar).toStrictEqual({
			text: 'An admin has disabled your account',
			color: 'info'
		});
	});

	it('ws connect is called on create', async () => {
		wrapper.vm.$store.commit = jest.fn();
		await wrapper.vm.$options.created[1].call(wrapper.vm);
		expect(wrapper.vm.$store.commit).toHaveBeenCalledWith('WS_CONNECT');
	});
});
