import Vue from 'vue';
import store from '@/store/store';
import websocketSetup from '@/backend/websockets';

websocketSetup();

describe('test store.ts', () => {
	beforeEach(() => {
		store.commit('RESET_STATE');
		store.state.http.get = jest.fn().mockImplementation(() =>
			Promise.resolve({
				data: [
					{ id: 400, description: 'Example 400' },
					{ description: 'Example' }
				]
			})
		);
	});

	it('test SOCKET_ONOPEN mutation', () => {
		store.commit('SOCKET_ONOPEN', { currentTarget: 'example' });
		expect(store.state.socket.isConnected).toStrictEqual(true);
	});

	it('test SOCKET_ONCLOSE mutation', () => {
		store.commit('SOCKET_ONCLOSE');
		expect(store.state.socket.isConnected).toStrictEqual(false);
	});

	it('test SOCKET_ONERROR mutation', () => {
		store.commit('SOCKET_ONERROR');
		expect(store.state.topBar).toStrictEqual({
			text: 'Lost connection to server, reconnecting...',
			color: 'error',
			active: true
		});
	});

	it('test SOCKET_ONMESSAGE mutation', () => {
		store.commit('SOCKET_ONMESSAGE', 'exampleMessage');
		expect(store.state.socket.message).toStrictEqual('exampleMessage');
	});

	it('test SOCKET_RECONNECT_ERROR mutation', () => {
		store.commit('SOCKET_RECONNECT_ERROR');
		expect(store.state.socket.reconnectError).toStrictEqual(true);
		expect(store.state.snackbar).toStrictEqual({
			text: 'There was an error reconnecting to the real-time backend server',
			color: 'error'
		});
	});

	it('SOCKET_RECONNECT mutation sets topbar inactive', () => {
		store.state.socket.isConnected = false;
		store.state.topBar.active = true;
		store.commit('SOCKET_RECONNECT');
		expect(store.state.socket.isConnected).toStrictEqual(true);
		expect(store.state.topBar.active).toStrictEqual(false);
	});

	it('test WS_CONNECT mutation', () => {
		const wsReconnect = jest.spyOn(Vue.prototype, '$disconnect');
		const wsConnect = jest.spyOn(Vue.prototype, '$connect');
		wsReconnect.mockResolvedValueOnce(true);
		expect(wsReconnect).toHaveBeenCalledTimes(0);
		expect(wsConnect).toHaveBeenCalledTimes(0);

		store.commit('WS_CONNECT');

		expect(wsReconnect).toHaveBeenCalledTimes(1);
		expect(wsConnect).toHaveBeenCalledWith('ws:///ws/main/?token=null');
	});

	it('test UPDATE_USER mutation where user has no id', () => {
		store.state.users = {};
		store.commit('UPDATE_USER', { username: 'test' });
		expect(store.state.users).toStrictEqual({});
	});

	it('test UPDATE_USER mutation where user is not logged in user', () => {
		store.state.users = {};
		store.dispatch('updateUser', {
			data: {
				id: 2,
				username: 'test',
				groups: [{ id: 2, name: 'group2' }]
			}
		});
		expect(store.state.users).toStrictEqual({
			'2': {
				id: 2,
				username: 'test',
				groups: [{ id: 2, name: 'group2' }]
			}
		});
	});

	it('test UPDATE_USER mutation where user is logged in user', () => {
		store.state.user = {
			id: 1,
			username: 'testUser',
			displayName: 'Test User',
			groups: [{ id: 4, name: 'group2', isActive: true }],
			isActive: true
		};
		store.state.users = {
			'1': {
				id: 1,
				username: 'testUser',
				displayName: 'Test User',
				groups: [{ id: 4, name: 'group2', isActive: true }],
				isActive: true
			}
		};
		store.commit('UPDATE_USER', {
			id: 1,
			username: 'test',
			groups: []
		});
		expect(store.state.users).toStrictEqual({
			'1': {
				id: 1,
				username: 'test',
				groups: [],
				isActive: undefined,
				displayName: undefined
			}
		});
		expect(store.state.user).toStrictEqual({
			id: 1,
			username: 'test',
			groups: [],
			displayName: undefined,
			isActive: undefined
		});
		store.commit('UPDATE_USER', {
			id: 1,
			username: 'new'
		});
		expect(store.state.user.groups).toStrictEqual([]);
	});

	it('UPDATE_GROUP sets group', () => {
		store.state.groups = {};
		store.dispatch('updateGroup', {
			data: { id: 2 }
		});
		expect(store.state.groups).toStrictEqual({ '2': { id: 2 } });
	});
});
