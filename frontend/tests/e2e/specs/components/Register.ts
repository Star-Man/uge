import { Selector } from 'testcafe';
import { seedDb, destroyDb, baseUrl, database } from '../../globals';
import { registerTab } from '../../generic-selectors/Auth';
import {
	url,
	inputValidationMessage,
	inputLabel,
	buttonLabel
} from '../../custom-commands';

const usernameInput = Selector('#registerUsernameInput');
const displayNameInput = Selector('#registerDisplayNameInput');
const passwordInput = Selector('#registerPasswordInput');
const confirmPasswordInput = Selector('#registerConfirmPasswordInput');
const registerButton = Selector('#registerButton');

fixture`components/Register Setup Database`
	.page(baseUrl)
	.beforeEach(browser =>
		seedDb().then(() => browser.resizeWindow(1280, 720).click(registerTab))
	)
	.afterEach(() => destroyDb());

test('registering as admin should redirect to user-settings', browser =>
	database()
		.raw('TRUNCATE TABLE authentication_customuser CASCADE')
		.then(() =>
			browser
				.typeText(usernameInput, 'testAdmin')
				.typeText(displayNameInput, 'Test Admin')
				.typeText(passwordInput, 'testPassword1')
				.typeText(confirmPasswordInput, 'testPassword1')
				.click(registerButton)
				.expect(url())
				.eql(`${baseUrl}/#/user-settings`)
		));

test('validation error should occur when attempting to register as existing user', browser =>
	browser
		.typeText(usernameInput, 'nonAdminGroupUser')
		.typeText(displayNameInput, 'Non Admin Group')
		.typeText(passwordInput, 'testPassword1')
		.typeText(confirmPasswordInput, 'testPassword1')
		.click(registerButton)
		.expect(inputValidationMessage(usernameInput))
		.eql('A user with that username already exists.'));

test('all text is correctly rendered', browser =>
	browser
		.expect(inputLabel(usernameInput))
		.eql('Username')
		.expect(inputLabel(displayNameInput))
		.eql('Display Name')
		.expect(inputLabel(passwordInput))
		.eql('Password')
		.expect(inputLabel(confirmPasswordInput))
		.eql('Confirm Password')
		.expect(buttonLabel(registerButton))
		.eql('Register'));

test('register button is disabled until all valid data is entered', browser =>
	browser
		.expect(registerButton.hasAttribute('disabled'))
		.ok()
		.typeText(usernameInput, 'testUser')
		.expect(registerButton.hasAttribute('disabled'))
		.ok()
		.typeText(displayNameInput, 'Test User')
		.expect(registerButton.hasAttribute('disabled'))
		.ok()
		.typeText(passwordInput, 'testPassword')
		.expect(registerButton.hasAttribute('disabled'))
		.ok()
		.typeText(confirmPasswordInput, 'testPassword')
		.expect(registerButton.hasAttribute('disabled'))
		.notOk());

test('pressing enter to register only works when data has been entered', browser =>
	browser
		.selectText(passwordInput)
		.pressKey('enter')
		.expect(url())
		.eql(`${baseUrl}/#/auth?nextUrl=%2F`));

test('register button disables after being enabled if data becomes invalid', browser =>
	browser
		.typeText(usernameInput, 'testUser')
		.typeText(displayNameInput, 'Test User')
		.typeText(passwordInput, 'testPassword')
		.typeText(confirmPasswordInput, 'testPassword')
		.expect(registerButton.hasAttribute('disabled'))
		.notOk()
		.selectText(usernameInput)
		.pressKey('delete')
		.expect(registerButton.hasAttribute('disabled'))
		.ok());

test('username input shows error when no username', browser =>
	browser
		.typeText(usernameInput, 'testUser1')
		.selectText(usernameInput)
		.pressKey('delete')
		.expect(inputValidationMessage(usernameInput))
		.eql('Username is required'));

test('display name input shows error when no username', browser =>
	browser
		.typeText(displayNameInput, 'Test User')
		.selectText(displayNameInput)
		.pressKey('delete')
		.expect(inputValidationMessage(displayNameInput))
		.eql('Display name is required'));

test('password input shows error when no password', browser =>
	browser
		.typeText(passwordInput, 'testUser1')
		.selectText(passwordInput)
		.pressKey('delete')
		.expect(inputValidationMessage(passwordInput))
		.eql('Password is required'));

test('password input shows error when passwords do not match', browser =>
	browser
		.typeText(passwordInput, 'password')
		.typeText(confirmPasswordInput, 'differentPassword')
		.expect(inputValidationMessage(passwordInput))
		.eql('Values do not match'));

test('password input shows error when passwords too short', browser =>
	browser
		.typeText(passwordInput, 'test')
		.typeText(confirmPasswordInput, 'test')
		.expect(inputValidationMessage(passwordInput))
		.eql('Password must be at least 6 characters'));

test('login button is disabled when password too short', browser =>
	browser
		.typeText(usernameInput, 'testUser1')
		.typeText(displayNameInput, 'Test User')
		.typeText(passwordInput, 'test')
		.typeText(confirmPasswordInput, 'test')
		.expect(registerButton.hasAttribute('disabled'))
		.ok()
		.typeText(passwordInput, '12')
		.typeText(confirmPasswordInput, '12')
		.expect(registerButton.hasAttribute('disabled'))
		.notOk());

test('pressing enter to submit is disabled when password too short', browser =>
	browser
		.typeText(usernameInput, 'testUser1')
		.typeText(displayNameInput, 'Test User')
		.typeText(passwordInput, 'test')
		.typeText(confirmPasswordInput, 'test')
		.selectText(confirmPasswordInput)
		.pressKey('enter')
		.expect(url())
		.eql(`${baseUrl}/#/auth?nextUrl=%2F`));

test('username field should be auto focused', browser => {
	return browser.pressKey('t e s t').expect(usernameInput.value).eql('test');
});
