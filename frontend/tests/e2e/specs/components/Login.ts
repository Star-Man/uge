import { seedDb, destroyDb, baseUrl } from '../../globals';
import { infoSnackbar, snackbarText } from '../../generic-selectors/Auth';
import {
	usernameInput,
	passwordInput,
	loginButton
} from '../../generic-selectors/Login';

import {
	inputLabel,
	buttonLabel,
	inputValidationMessage,
	url
} from '../../custom-commands';

fixture`components/Login Setup Database`
	.page(baseUrl)
	.beforeEach(() => seedDb())
	.afterEach(() => destroyDb());

test('logging in as admin should redirect to user-settings', browser =>
	browser
		.typeText(usernameInput, 'testAdmin')
		.typeText(passwordInput, 'testPassword1')
		.click(loginButton)
		.expect(url())
		.eql(`${baseUrl}/#/user-settings`));

test('login with wrong password should display notification', browser =>
	browser
		.typeText(usernameInput, 'testIncorrectUser')
		.typeText(passwordInput, 'testIncorrectPassword')
		.click(loginButton)
		.expect(infoSnackbar.exists)
		.ok()
		.expect(snackbarText.textContent)
		.contains('Unable to log in with provided credentials.'));

test('all text is correctly rendered', browser =>
	browser
		.expect(inputLabel(usernameInput))
		.eql('Username')
		.expect(inputLabel(passwordInput))
		.eql('Password')
		.expect(buttonLabel(loginButton))
		.eql('Login'));

test('login button only appears when data has been entered', browser =>
	browser
		.expect(loginButton.hasAttribute('disabled'))
		.ok()
		.typeText(usernameInput, 'testUser1')
		.typeText(passwordInput, 'testPassword1')
		.expect(loginButton.hasAttribute('disabled'))
		.notOk()
		.selectText(passwordInput)
		.pressKey('delete')
		.expect(loginButton.hasAttribute('disabled'))
		.ok());

test('pressing enter to login only works when data has been entered', browser =>
	browser
		.selectText(passwordInput)
		.pressKey('enter')
		.expect(infoSnackbar.exists)
		.notOk()
		.typeText(usernameInput, 'testUser1')
		.typeText(passwordInput, 'testPassword1')
		.selectText(passwordInput)
		.pressKey('enter')
		.expect(infoSnackbar.exists)
		.ok());

test('username input shows error when no username', browser =>
	browser
		.typeText(usernameInput, 'testUser1')
		.selectText(usernameInput)
		.pressKey('delete')
		.expect(inputValidationMessage(usernameInput))
		.eql('Username is required'));

test('password input shows error when no password', browser =>
	browser
		.typeText(passwordInput, 'testUser1')
		.selectText(passwordInput)
		.pressKey('delete')
		.expect(inputValidationMessage(passwordInput))
		.eql('Password is required'));

test('password input shows error when password too short', browser =>
	browser
		.typeText(passwordInput, 'test')
		.expect(inputValidationMessage(passwordInput))
		.eql('Password must be at least 6 characters'));

test('login button is disabled when password too short', browser =>
	browser
		.typeText(usernameInput, 'testUser1')
		.typeText(passwordInput, 'test')
		.expect(loginButton.hasAttribute('disabled'))
		.ok()
		.typeText(passwordInput, '12')
		.expect(loginButton.hasAttribute('disabled'))
		.notOk());

test('pressing enter to submit is disabled when password too short', browser =>
	browser
		.typeText(usernameInput, 'testUser1')
		.typeText(passwordInput, 'test')
		.selectText(passwordInput)
		.pressKey('enter')
		.expect(infoSnackbar.exists)
		.notOk()
		.typeText(passwordInput, '12')
		.selectText(passwordInput)
		.pressKey('enter')
		.expect(url())
		.eql(`${baseUrl}/#/auth?nextUrl=%2F`));

test('username field should be auto focused', browser =>
	browser.pressKey('t e s t').expect(usernameInput.value).eql('test'));
