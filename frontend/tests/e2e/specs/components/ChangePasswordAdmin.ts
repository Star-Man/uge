import { Selector } from 'testcafe';
import {
	admin,
	inputLabel,
	inputValidationMessage
} from '../../custom-commands';
import { userTableRow1EditButton } from '../../generic-selectors/Admin';
import { seedDb, destroyDb, baseUrl } from '../../globals';
import { infoSnackbar, snackbarText } from '../../generic-selectors/Auth';

import { loginRequest } from '../../generic-functions';

const openChangePasswordDialogButton = Selector(
	'#openChangePasswordDialogButton'
);
const passwordChangeForm = Selector('#passwordChangeForm');
const newPasswordInput = Selector('#passwordInput');
const confirmPasswordInput = Selector('#confirmPasswordInput');
const changePasswordButton = Selector('#changePasswordButton');

fixture`components/ChangePasswordAdmin Setup Database`
	.page(`${baseUrl}/#/`)
	.beforeEach(async browser => {
		await seedDb();
		await admin();
		return browser
			.navigateTo(`${baseUrl}/#/admin`)
			.click(userTableRow1EditButton);
	})
	.afterEach(() => destroyDb());

test('change password form opens on button click', browser =>
	browser
		.expect(passwordChangeForm.exists)
		.notOk()
		.click(openChangePasswordDialogButton)
		.expect(passwordChangeForm.visible)
		.ok());

test('change password dialog closes by pressing escape', browser =>
	browser
		.click(openChangePasswordDialogButton)
		.pressKey('esc')
		.expect(passwordChangeForm.visible)
		.notOk());

test('all text is correctly rendered', browser =>
	browser
		.click(openChangePasswordDialogButton)
		.expect(inputLabel(newPasswordInput))
		.eql('New Password')
		.expect(inputLabel(confirmPasswordInput))
		.eql('Confirm New Password'));

test('changing password updates a users password', browser =>
	browser
		.click(openChangePasswordDialogButton)
		.typeText(newPasswordInput, 'newPassword')
		.typeText(confirmPasswordInput, 'newPassword')
		.click(changePasswordButton)
		.then(() =>
			loginRequest('nonAdminGroupUser', 'newPassword').then(response =>
				browser
					.expect(typeof response.data.key)
					.eql('string')
					.expect(response.data.user.username)
					.eql('nonAdminGroupUser')
			)
		));

test('change password form shows no errors on load', browser =>
	browser
		.click(openChangePasswordDialogButton)
		.expect(
			newPasswordInput
				.parent()
				.parent()
				.parent()
				.find('.v-messages__message').exists
		)
		.notOk()
		.expect(
			confirmPasswordInput
				.parent()
				.parent()
				.parent()
				.find('.v-messages__message').exists
		)
		.notOk());

test('new password field should be auto focused', browser => {
	return browser
		.click(openChangePasswordDialogButton)
		.pressKey('t e s t')
		.expect(newPasswordInput.value)
		.eql('test');
});

test('new password input shows error when no password', browser =>
	browser
		.click(openChangePasswordDialogButton)
		.typeText(newPasswordInput, '1')
		.selectText(newPasswordInput)
		.pressKey('delete')
		.expect(inputValidationMessage(newPasswordInput))
		.eql('New Password is required'));

test('confirm password input shows error when no password', browser =>
	browser
		.click(openChangePasswordDialogButton)
		.typeText(confirmPasswordInput, '1')
		.selectText(confirmPasswordInput)
		.pressKey('delete')
		.expect(inputValidationMessage(confirmPasswordInput))
		.eql('Password confirmation is required'));

test('new password input shows error when password is too short', browser =>
	browser
		.click(openChangePasswordDialogButton)
		.typeText(newPasswordInput, 'test')
		.expect(inputValidationMessage(newPasswordInput))
		.eql('Password must be at least 6 characters'));

test('confirm password input shows error when password is too short', browser =>
	browser
		.click(openChangePasswordDialogButton)
		.typeText(confirmPasswordInput, 'test')
		.expect(inputValidationMessage(confirmPasswordInput))
		.eql('Password must be at least 6 characters'));

test('new password input shows error when passwords do not match', browser =>
	browser
		.click(openChangePasswordDialogButton)
		.typeText(newPasswordInput, 'password')
		.typeText(confirmPasswordInput, 'differentPassword')
		.expect(inputValidationMessage(newPasswordInput))
		.eql('Values do not match'));

test('changing password successfully closes dialog', browser =>
	browser
		.click(openChangePasswordDialogButton)
		.typeText(newPasswordInput, 'newPassword')
		.typeText(confirmPasswordInput, 'newPassword')
		.click(changePasswordButton)
		.expect(passwordChangeForm.visible)
		.notOk());

test('opening password change form after save will reset inputs', browser =>
	browser
		.click(openChangePasswordDialogButton)
		.typeText(newPasswordInput, 'newPassword')
		.typeText(confirmPasswordInput, 'newPassword')
		.click(changePasswordButton)
		.click(openChangePasswordDialogButton)
		.expect(newPasswordInput.value)
		.eql('')
		.expect(confirmPasswordInput.value)
		.eql(''));

test('opening password change form after save will reset validation', browser =>
	browser
		.click(openChangePasswordDialogButton)
		.typeText(newPasswordInput, 'newPassword')
		.typeText(confirmPasswordInput, 'newPassword')
		.click(changePasswordButton)
		.click(openChangePasswordDialogButton)
		.expect(
			newPasswordInput
				.parent()
				.parent()
				.parent()
				.find('.v-messages__message').exists
		)
		.notOk()
		.expect(
			confirmPasswordInput
				.parent()
				.parent()
				.parent()
				.find('.v-messages__message').exists
		)
		.notOk());

test('snackbar should appear on successful password change', browser =>
	browser
		.click(openChangePasswordDialogButton)
		.typeText(newPasswordInput, 'newPassword')
		.typeText(confirmPasswordInput, 'newPassword')
		.click(changePasswordButton)
		.expect(infoSnackbar.exists)
		.ok()
		.expect(snackbarText.textContent)
		.contains('Password changed!'));

test('change password button is disabled until all data is entered', browser =>
	browser
		.click(openChangePasswordDialogButton)
		.expect(changePasswordButton.hasAttribute('disabled'))
		.ok()
		.typeText(newPasswordInput, 'newPassword')
		.expect(changePasswordButton.hasAttribute('disabled'))
		.ok()
		.typeText(confirmPasswordInput, 'newPassword')
		.expect(changePasswordButton.hasAttribute('disabled'))
		.notOk());

test('change password button disables after data becoming invalid', browser =>
	browser
		.click(openChangePasswordDialogButton)
		.typeText(newPasswordInput, 'newPassword')
		.typeText(confirmPasswordInput, 'newPassword')
		.expect(changePasswordButton.hasAttribute('disabled'))
		.notOk()
		.selectText(newPasswordInput)
		.pressKey('delete')
		.expect(changePasswordButton.hasAttribute('disabled'))
		.ok());

test('change password can be done by pressing enter', browser =>
	browser
		.click(openChangePasswordDialogButton)
		.typeText(newPasswordInput, 'newPassword')
		.typeText(confirmPasswordInput, 'newPassword')
		.pressKey('enter')
		.expect(passwordChangeForm.visible)
		.notOk());

test('pressing enter to submit does not work when data is invalid', browser =>
	browser
		.click(openChangePasswordDialogButton)
		.typeText(newPasswordInput, 'newPassword')
		.typeText(confirmPasswordInput, 'newPassword')
		.selectText(newPasswordInput)
		.pressKey('delete')
		.pressKey('enter')
		.expect(passwordChangeForm.visible)
		.ok());
