import { Selector } from 'testcafe';
import { seedDb, destroyDb, baseUrl } from '../../globals';
import { changeBackendPath } from '../../generic-functions';

const topBar = Selector('#topBar');
const topBarText = Selector('#topBarText');

fixture`components/TopBar Setup Database`
	.page(baseUrl)
	.beforeEach(() => seedDb())
	.beforeEach(async browser => {
		await seedDb();
		return browser.resizeWindow(1280, 720);
	})
	.afterEach(() => destroyDb());

test('top bar does not appear on load', browser =>
	browser.expect(topBar.visible).notOk());

test('top bar appears when websocket disconnects', async browser => {
	await browser.navigateTo(`${baseUrl}/#/`);
	await changeBackendPath('wrong-path');
	await browser
		.expect(topBarText.textContent)
		.eql('Lost connection to server, reconnecting...')
		.expect(topBar.getStyleProperty('background-color'))
		.eql('rgb(255, 82, 82)');
});

test('top bar disappears when websocket reconnects', async browser => {
	await browser.navigateTo(`${baseUrl}/#/`);
	await changeBackendPath('wrong-path');
	await browser.expect(topBar.visible).ok();
	await changeBackendPath(
		`${process.env.VUE_APP_API_HOST}:${process.env.VUE_APP_API_PORT}`
	);
	await browser.expect(topBar.exists).notOk();
});
