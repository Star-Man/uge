import { Selector } from 'testcafe';
import {
	infoSnackbar,
	snackbarText,
	snackbarButton
} from '../../../generic-selectors/Auth';
import {
	groupTable,
	groupTableRows,
	groupTableRow1
} from '../../../generic-selectors/Admin';
import { seedDb, destroyDb, baseUrl } from '../../../globals';
import { admin } from '../../../custom-commands';
import {
	createGroup,
	updateGroup,
	getGroupByName
} from '../../../generic-functions';

const groupEditNameField = Selector('#groupEditNameField');
const addGroupButton = Selector('#addGroupButton');
const groupTableRow2 = groupTableRows.nth(2);
const groupCreateDescriptionField = Selector('#groupCreateDescriptionField');
const groupTableHeader1 = groupTable.find('th').nth(0).child('span');
const groupTableHeader2 = groupTable.find('th').nth(1).child('span');
const groupTableRow1Col1 = groupTableRow1.child('td').nth(0);
const groupTableRow1Col2 = groupTableRow1.child('td').nth(1);
const groupTableRow2Col1 = groupTableRow2.child('td').nth(0);
const groupTableRow2Col2 = groupTableRow2.child('td').nth(1);
const groupTableRow1DeleteButton = groupTableRow1Col2.find('svg').nth(0);
const groupTableRow2DeleteButton = groupTableRow2Col2.find('svg').nth(0);

fixture`components/Admin/GroupList Setup Database`
	.page(`${baseUrl}/#/`)
	.beforeEach(async browser => {
		await seedDb();
		await browser.resizeWindow(1280, 720);
	})
	.afterEach(() => destroyDb());

test('group table headers are visible', async browser => {
	await admin();
	await browser
		.navigateTo(`${baseUrl}/#/admin`)
		.expect(groupTableHeader1.textContent)
		.eql('Name')
		.expect(groupTableHeader2.textContent)
		.eql('Actions');
});

test('group table data is visible', async browser => {
	await admin();
	await browser
		.navigateTo(`${baseUrl}/#/admin`)
		.expect(groupTableRows.count)
		.eql(3)
		.expect(groupTableRow1Col1.textContent)
		.eql('admin');
});

test('group actions are not visible for current group', async browser => {
	await admin();
	await browser
		.navigateTo(`${baseUrl}/#/admin`)
		.expect(groupTableRow1DeleteButton.exists)
		.notOk()
		.expect(groupTableRow2DeleteButton.visible)
		.ok();
});

test('group list updates when a new group is created', async browser => {
	await admin();
	await browser
		.navigateTo(`${baseUrl}/#/admin`)
		.expect(groupTableRows.count)
		.eql(3);
	await createGroup('aaaa');
	await browser
		.expect(groupTableRows.count)
		.eql(4)
		.expect(groupTableRow1Col1.textContent)
		.eql(' aaaa ');
});

test('notification is shown on group deletion', async browser => {
	await admin();
	await browser
		.navigateTo(`${baseUrl}/#/admin`)
		.click(groupTableRow2DeleteButton)
		.expect(infoSnackbar.visible)
		.ok()
		.expect(snackbarText.textContent)
		.contains('nonAdminGroup has been deleted');
});

test('undo button is shown on group deletion', async browser => {
	await admin();
	await browser
		.navigateTo(`${baseUrl}/#/admin`)
		.click(groupTableRow2DeleteButton)
		.expect(snackbarButton.visible)
		.ok()
		.expect(snackbarButton.textContent)
		.contains(' Undo ');
});

test('deleting group removes them from group list', async browser => {
	await admin();
	await browser
		.navigateTo(`${baseUrl}/#/admin`)
		.click(groupTableRow2DeleteButton)
		.expect(groupTableRows.count)
		.eql(2);
});

test('deleting group marks them inactive in database after 5 secs', async browser => {
	await admin();
	await browser
		.navigateTo(`${baseUrl}/#/admin`)
		.click(groupTableRow2DeleteButton);
	await getGroupByName('nonAdminGroup').then(group =>
		browser.expect(group[0].isActive).eql(true)
	);
	await browser.wait(5100);
	await getGroupByName('nonAdminGroup').then(group =>
		browser.expect(group[0].isActive).eql(false)
	);
});

test('undo button makes group active in database', async browser => {
	await admin();
	await browser
		.navigateTo(`${baseUrl}/#/admin`)
		.click(groupTableRow2DeleteButton)
		.click(snackbarButton)
		.then(() =>
			getGroupByName('nonAdminGroup').then(group =>
				browser.expect(group[0].isActive).eql(true)
			)
		);
});

test('undo button adds group back to group list', async browser => {
	await admin();
	await browser
		.navigateTo(`${baseUrl}/#/admin`)
		.click(groupTableRow2DeleteButton)
		.click(snackbarButton)
		.expect(groupTableRows.count)
		.eql(3);
});

test('group list updates when group deleted elsewhere', async browser => {
	await admin();
	await browser.navigateTo(`${baseUrl}/#/admin`);
	await updateGroup('nonAdminGroup', { isActive: false });
	await browser.expect(groupTableRows.count).eql(2);
});

test('groups update in database on change', async browser => {
	await admin();
	return browser
		.navigateTo(`${baseUrl}/#/admin`)
		.click(groupTableRow2Col1)
		.selectText(groupEditNameField)
		.pressKey('delete')
		.typeText(groupEditNameField, 'New Group')
		.click(groupTableHeader1)
		.then(() =>
			getGroupByName('New Group').then(groups =>
				browser
					.expect(
						groups.filter(
							(group: { name: string }) =>
								group.name === 'New Group'
						).length
					)
					.gte(1)
			)
		);
});

test('added group is saved in database', async browser => {
	await admin();
	return browser
		.navigateTo(`${baseUrl}/#/admin`)
		.click(addGroupButton)
		.typeText(groupCreateDescriptionField, 'New Group')
		.click(groupTableHeader1)
		.then(() =>
			getGroupByName('New Group').then(groups =>
				browser
					.expect(
						groups.filter(
							(group: { name: string }) =>
								group.name === 'New Group'
						).length
					)
					.gte(1)
			)
		);
});

test('field is added on button click', async browser => {
	await admin();
	return browser
		.navigateTo(`${baseUrl}/#/admin`)
		.click(addGroupButton)
		.typeText(groupCreateDescriptionField, 'New Group')
		.click(groupTableHeader1)
		.expect(groupTableRows.count)
		.eql(4);
});

test('group is saved on pressing enter', async browser => {
	await admin();
	return browser
		.navigateTo(`${baseUrl}/#/admin`)
		.click(addGroupButton)
		.typeText(groupCreateDescriptionField, 'New Group')
		.pressKey('enter')
		.then(() =>
			getGroupByName('New Group').then(groups =>
				browser
					.expect(
						groups.filter(
							(group: { name: string }) =>
								group.name === 'New Group'
						).length
					)
					.gte(1)
			)
		);
});
