import { Selector } from 'testcafe';
import {
	infoSnackbar,
	snackbarText,
	snackbarButton
} from '../../../generic-selectors/Auth';
import {
	userTable,
	userTableRows,
	userTableRow1,
	userTableRow1Col3,
	userTableRow1EditButton
} from '../../../generic-selectors/Admin';
import { seedDb, destroyDb, baseUrl } from '../../../globals';
import { admin } from '../../../custom-commands';
import {
	getUserByName,
	createUser,
	updateUser,
	getGroupByName
} from '../../../generic-functions';

const userTableHeader1 = userTable.find('th').nth(0).child('span');
const userTableHeader2 = userTable.find('th').nth(1).child('span');
const userTableHeader3 = userTable.find('th').nth(2).child('span');
const userTableHeader4 = userTable.find('th').nth(3).child('span');
const userTableRow2 = userTableRows.nth(2);
const userTableRow1Col1 = userTableRow1.child('td').nth(0);
const userTableRow2Col3 = userTableRow1.child('td').nth(2);
const userTableRow1Col4 = userTableRow1.child('td').nth(3);
const userTableRow2Col4 = userTableRow2.child('td').nth(3);
const userTableRow1Col2 = userTableRow1.child('td').nth(1);
const userTableRow2EditButton = userTableRow2Col3.find('svg').nth(0);
const userTableRow1DeleteButton = userTableRow1Col4.find('svg').nth(1);
const userTableRow2DeleteButton = userTableRow2Col4.find('svg').nth(1);
const editUserDialogHeader = Selector('#editUserDialogHeader');
const editUserDialogGroupInput = Selector('#editUserDialogGroupInput');
const editUserDialogGroupInputText = editUserDialogGroupInput.sibling('div');

const loginForm = Selector('#loginForm');

fixture`components/Admin/UserList Setup Database`
	.page(`${baseUrl}/#/`)
	.beforeEach(async browser => {
		await seedDb();
		await browser.resizeWindow(1280, 720);
	})
	.afterEach(() => destroyDb());

test('user table headers are visible', async browser => {
	await admin();
	await browser
		.navigateTo(`${baseUrl}/#/admin`)
		.expect(userTableHeader1.textContent)
		.eql('Name')
		.expect(userTableHeader2.textContent)
		.eql('Username')
		.expect(userTableHeader3.textContent)
		.eql('Groups')
		.expect(userTableHeader4.textContent)
		.eql('Actions');
});

test('user table data is visible', async browser => {
	await admin();
	await browser
		.navigateTo(`${baseUrl}/#/admin`)
		.expect(userTableRows.count)
		.eql(3)
		.expect(userTableRow1Col1.textContent)
		.eql('Test Non Admin User')
		.expect(userTableRow1Col2.textContent)
		.eql('nonAdminGroupUser')
		.expect(userTableRow1Col3.textContent)
		.eql('nonAdminGroup');
});

test('user actions are not visible for current user', async browser => {
	await admin();
	await browser
		.navigateTo(`${baseUrl}/#/admin`)
		.expect(userTableRow2EditButton.exists)
		.notOk()
		.expect(userTableRow2DeleteButton.exists)
		.notOk()
		.expect(userTableRow1EditButton.visible)
		.ok()
		.expect(userTableRow1DeleteButton.visible)
		.ok();
});
test('pressing edit button for user opens edit dialog', async browser => {
	await admin();
	await browser
		.navigateTo(`${baseUrl}/#/admin`)
		.click(userTableRow1EditButton)
		.expect(editUserDialogHeader.textContent)
		.eql('Edit Test Non Admin User')
		.expect(editUserDialogGroupInput.visible)
		.ok()
		.expect(editUserDialogGroupInputText.textContent)
		.eql('nonAdminGroup');
});

test('user list updates when a new user is created', async browser => {
	await admin();
	await browser
		.navigateTo(`${baseUrl}/#/admin`)
		.expect(userTableRows.count)
		.eql(3);
	await createUser();
	await browser
		.expect(userTableRows.count)
		.eql(4)
		.expect(userTableRow1Col1.textContent)
		.eql('New Test User');
});

test('user list updates when a users group is updated', async browser => {
	await admin();
	await browser
		.navigateTo(`${baseUrl}/#/admin`)
		.expect(userTableRow2Col3.textContent)
		.eql('nonAdminGroup');
	const [adminGroup] = await getGroupByName('admin');
	const [nonAdminGroup] = await getGroupByName('nonAdminGroup');
	await updateUser('nonAdminGroupUser', {
		groups: [adminGroup.id, nonAdminGroup.id]
	});
	await browser
		.expect(userTableRow2Col3.textContent)
		.eql('nonAdminGroup,admin');
});

test('notification is shown on user deletion', async browser => {
	await admin();
	await browser
		.navigateTo(`${baseUrl}/#/admin`)
		.click(userTableRow1DeleteButton)
		.expect(infoSnackbar.visible)
		.ok()
		.expect(snackbarText.textContent)
		.contains('Test Non Admin User has been deleted');
});

test('undo button is shown on user deletion', async browser => {
	await admin();
	await browser
		.navigateTo(`${baseUrl}/#/admin`)
		.click(userTableRow1DeleteButton)
		.expect(snackbarButton.visible)
		.ok()
		.expect(snackbarButton.textContent)
		.contains(' Undo ');
});

test('deleting user removes them from user list', async browser => {
	await admin();
	await browser
		.navigateTo(`${baseUrl}/#/admin`)
		.click(userTableRow1DeleteButton)
		.expect(userTableRows.count)
		.eql(2);
});

test('deleting user marks them inactive in database after 5 secs', async browser => {
	await admin();
	await browser
		.navigateTo(`${baseUrl}/#/admin`)
		.click(userTableRow1DeleteButton);
	await getUserByName('nonAdminGroupUser').then(user =>
		browser.expect(user[0].isActive).eql(true)
	);
	await browser.wait(5100);
	await getUserByName('nonAdminGroupUser').then(user =>
		browser.expect(user[0].isActive).eql(false)
	);
});

test('undo button makes user active in database', async browser => {
	await admin();
	await browser
		.navigateTo(`${baseUrl}/#/admin`)
		.click(userTableRow1DeleteButton)
		.click(snackbarButton)
		.then(() =>
			getUserByName('nonAdminGroupUser').then(user =>
				browser.expect(user[0].isActive).eql(true)
			)
		);
});

test('undo button adds user back to user list', async browser => {
	await admin();
	await browser
		.navigateTo(`${baseUrl}/#/admin`)
		.click(userTableRow1DeleteButton)
		.click(snackbarButton)
		.expect(userTableRows.count)
		.eql(3);
});

test('user list updates when user deleted elsewhere', async browser => {
	await admin();
	await browser.navigateTo(`${baseUrl}/#/admin`);
	await updateUser('nonAdminGroupUser', { isActive: false });
	await browser.expect(userTableRows.count).eql(2);
});

test('deleting a user logs them out', async browser => {
	await admin();
	await browser.navigateTo(`${baseUrl}/#/admin`);
	await updateUser('testAdmin', { isActive: false });
	await browser
		.expect(loginForm.visible)
		.ok()
		.expect(infoSnackbar.visible)
		.ok()
		.expect(snackbarText.textContent)
		.contains('An admin has disabled your account');
});
