import { Selector } from 'testcafe';
import { seedDb, destroyDb, baseUrl } from '../../globals';
import { getGroupByName, updateUser } from '../../generic-functions';
import { admin, nonAdminGroupUser, url } from '../../custom-commands';

import { infoSnackbar, snackbarText } from '../../generic-selectors/Auth';
import { mySettingsLink } from '../../generic-selectors/Sidebar';

const sidebar = Selector('#sidebar');
const adminLink = Selector('#sidebar-title-admin');

const mySettingsIcon = Selector('#sidebar-icon-user-settings');
const mySettingsIconText = mySettingsIcon.child();
const adminIcon = Selector('#sidebar-icon-admin');
const logoutIcon = Selector('#sidebar-icon-logout');
const sidebarGroup = Selector('#sidebar-group');
const mySettingsPage = Selector('#mySettingsPage');
const adminPage = Selector('#adminPage');
const topNavBar = Selector('#topNavBar');
const topNavBarDrawerOpenButton = Selector('#topNavBarDrawerOpenButton');

fixture`components/Sidebar Setup Database`
	.page(baseUrl)
	.beforeEach(async browser => {
		await seedDb();
		return browser.resizeWindow(1280, 720);
	})
	.afterEach(() => destroyDb());

test('admin can view admin link in sidebar', async browser => {
	await admin();
	return browser
		.expect(sidebarGroup.childElementCount)
		.eql(3)
		.expect(adminLink.exists)
		.ok();
});

test('nonAdminGroup can not view admin link in sidebar', async browser => {
	await nonAdminGroupUser();
	return browser
		.expect(sidebarGroup.childElementCount)
		.eql(2)
		.expect(adminLink.exists)
		.notOk();
});

test('sidebar text is visible', async browser => {
	await admin();
	return browser
		.expect(mySettingsLink.visible)
		.ok()
		.expect(mySettingsLink.textContent)
		.eql('Test Admin')
		.expect(adminLink.visible)
		.ok()
		.expect(adminLink.textContent)
		.eql('Admin');
});

test('sidebar icons are visible', async browser => {
	await admin();
	return browser
		.expect(mySettingsIcon.visible)
		.ok()
		.expect(adminIcon.visible)
		.ok();
});

test('avatar icon uses first letter of username', async browser => {
	await admin();
	return browser.expect(mySettingsIconText.textContent).eql('T');
});

test('sidebar can expand on hover', async browser => {
	await admin();
	return browser
		.expect(sidebar.getStyleProperty('width'))
		.eql('56px')
		.hover(sidebar)
		.expect(sidebar.getStyleProperty('width'))
		.eql('256px');
});

test('clicking the my settings link opens the my settings page', async browser => {
	await admin();
	return browser
		.click(mySettingsIcon)
		.expect(url())
		.eql(`${baseUrl}/#/user-settings`)
		.expect(mySettingsPage.visible)
		.ok();
});

test('clicking the admin link opens the admin page', async browser => {
	await admin();
	return browser
		.click(adminIcon)
		.expect(url())
		.eql(`${baseUrl}/#/admin`)
		.expect(adminPage.visible)
		.ok();
});

test('clicking the logout link opens logs user out', async browser => {
	await admin();
	return browser.click(logoutIcon).expect(url()).eql(`${baseUrl}/#/auth`);
});

test('notification is shown when user group updated by admin', async browser => {
	await admin();
	await browser.expect(infoSnackbar.exists).notOk();
	const [nonAdminGroup] = await getGroupByName('nonAdminGroup');
	await updateUser('testAdmin', { groups: [nonAdminGroup.id] });
	await browser
		.expect(infoSnackbar.exists)
		.ok()
		.expect(snackbarText.textContent)
		.eql('An admin has changed your groups');
});

test('admin option disappears when user group changed to nonAdminGroup', async browser => {
	await admin();
	await browser
		.expect(sidebarGroup.childElementCount)
		.eql(3)
		.expect(adminLink.exists)
		.ok();
	const [nonAdminGroup] = await getGroupByName('nonAdminGroup');
	await updateUser('testAdmin', { groups: [nonAdminGroup.id] });
	await browser
		.expect(sidebarGroup.childElementCount)
		.eql(2)
		.expect(adminLink.exists)
		.notOk();
});

test('admin option appears when user group changed to admin', async browser => {
	await nonAdminGroupUser();
	await browser
		.expect(sidebarGroup.childElementCount)
		.eql(2)
		.expect(adminLink.exists)
		.notOk();
	const [adminGroup] = await getGroupByName('admin');
	await updateUser('nonAdminGroupUser', { groups: [adminGroup.id] });
	await browser
		.wait(10000)
		.expect(sidebarGroup.childElementCount)
		.eql(3)
		.expect(adminLink.exists)
		.ok();
});

test('user redirected to userSettings page from admin page if group changed', async browser => {
	await admin();
	await browser.click(adminIcon);
	const [nonAdminGroup] = await getGroupByName('nonAdminGroup');
	await updateUser('testAdmin', { groups: [nonAdminGroup.id] });
	await browser.expect(url()).eql(`${baseUrl}/#/user-settings`);
});

test('user not redirected on group change if not on admin page', async browser => {
	await admin();
	await browser.click(mySettingsIcon);
	const [nonAdminGroup] = await getGroupByName('nonAdminGroup');
	await updateUser('testAdmin', { groups: [nonAdminGroup.id] });
	await browser.expect(url()).eql(`${baseUrl}/#/user-settings`);
});

test('sidebar is not visible in xs breakpoint', async browser => {
	await admin();
	return browser
		.expect(sidebar.visible)
		.ok()
		.resizeWindow(480, 640)
		.expect(sidebar.visible)
		.notOk();
});

test('top nav bar is not visible in normal breakpoints', async browser => {
	await admin();
	return browser.expect(topNavBar.visible).notOk();
});

test('top nav bar is visible in xs breakpoint', async browser => {
	await admin();
	return browser.resizeWindow(480, 640).expect(topNavBar.visible).ok();
});

test('top nav bar button opens sidebar', async browser => {
	await admin();
	return browser
		.resizeWindow(480, 640)
		.click(topNavBarDrawerOpenButton)
		.expect(sidebar.visible)
		.ok();
});

test('clicking sidebar link in xs breakpoint closes sidebar', async browser => {
	await admin();
	return browser
		.resizeWindow(480, 640)
		.click(topNavBarDrawerOpenButton)
		.click(adminLink)
		.expect(sidebar.visible)
		.notOk();
});
