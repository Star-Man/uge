import { Selector } from 'testcafe';
import { seedDb, destroyDb, baseUrl } from '../../globals';
import { getUserByName, updateUser } from '../../generic-functions';
import { mySettingsLink } from '../../generic-selectors/Sidebar';
import {
	inputLabel,
	admin,
	nonAdminGroupUser,
	inputValidationMessage
} from '../../custom-commands';

const sidebar = Selector('#sidebar');
const displayNameInput = Selector('#displayNameInput');

fixture`views/userSettings Setup Database`
	.page(`${baseUrl}/#/user-settings`)
	.beforeEach(async browser => {
		await seedDb();
		await browser.resizeWindow(1280, 720);
	})
	.afterEach(() => destroyDb());

test('sidebar is visible', async browser => {
	await admin();
	return browser.expect(sidebar.visible).ok();
});

test('display name input is visible', async browser => {
	await nonAdminGroupUser();
	return browser
		.navigateTo(`${baseUrl}/#/user-settings`)
		.expect(displayNameInput.visible)
		.ok()
		.expect(inputLabel(displayNameInput))
		.eql('Display Name');
});

test('display name input updates database on enter key', async browser => {
	await nonAdminGroupUser();
	return browser
		.navigateTo(`${baseUrl}/#/user-settings`)
		.selectText(displayNameInput)
		.pressKey('delete')
		.typeText(displayNameInput, 'New Display Name')
		.pressKey('enter')
		.then(() =>
			getUserByName('nonAdminGroupUser').then(user =>
				browser.expect(user[0].displayName).eql('New Display Name')
			)
		);
});

test('display name input shows current user display name on load', async browser => {
	await nonAdminGroupUser();
	return browser
		.navigateTo(`${baseUrl}/#/user-settings`)
		.expect(displayNameInput.value)
		.eql('Test Non Admin User');
});

test('updating display name updates name in menu', async browser => {
	await nonAdminGroupUser();
	return browser
		.navigateTo(`${baseUrl}/#/user-settings`)
		.selectText(displayNameInput)
		.pressKey('delete')
		.typeText(displayNameInput, 'New Display Name 2')
		.pressKey('enter')
		.expect(mySettingsLink.textContent)
		.eql('New Display Name 2');
});

test('display name input shows warning on empty', async browser => {
	await nonAdminGroupUser();
	return browser
		.navigateTo(`${baseUrl}/#/user-settings`)
		.selectText(displayNameInput)
		.pressKey('delete')
		.expect(inputValidationMessage(displayNameInput))
		.eql("Display name can't be blank");
});

test("display name input doesn't update name when empty", async browser => {
	await nonAdminGroupUser();
	return browser
		.navigateTo(`${baseUrl}/#/user-settings`)
		.selectText(displayNameInput)
		.pressKey('delete')
		.pressKey('enter')
		.then(() =>
			getUserByName('nonAdminGroupUser').then(user =>
				browser.expect(user[0].displayName).eql('Test Non Admin User')
			)
		);
});

test('display name input updates when user updated elsewhere', async browser => {
	await nonAdminGroupUser();
	await browser.navigateTo(`${baseUrl}/#/user-settings`);
	await updateUser('nonAdminGroupUser', {
		displayName: 'New Display Name 5'
	});
	await browser.expect(displayNameInput.value).eql('New Display Name 5');
});
