import { Selector } from 'testcafe';
import { seedDb, destroyDb, baseUrl } from '../../globals';
import { url, admin, nonAdminGroupUser } from '../../custom-commands';

const sidebar = Selector('#sidebar');

fixture`views/Admin Setup Database`
	.page(`${baseUrl}/#/`)
	.beforeEach(async browser => {
		await seedDb();
		await browser.resizeWindow(1280, 720);
	})
	.afterEach(() => destroyDb());

test('page cannot be accessed by nonAdminGroupUser', async browser => {
	await nonAdminGroupUser();
	return browser
		.navigateTo(`${baseUrl}/#/admin`)
		.expect(url())
		.eql(`${baseUrl}/#/user-settings`);
});

test('sidebar is visible', async browser => {
	await admin();
	return browser.expect(sidebar.visible).ok();
});
