export default (input: Selector): Promise<string> => {
	return input.child('div').child('div').child('div').child('label')
		.textContent;
};
