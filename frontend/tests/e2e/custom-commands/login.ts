import { t } from 'testcafe';
import {
	usernameInput,
	passwordInput,
	loginButton
} from '../generic-selectors/Login';

export const nonAdminGroupUser = (): TestController =>
	t
		.typeText(usernameInput, 'nonAdminGroupUser')
		.typeText(passwordInput, 'testPassword1')
		.click(loginButton);

export const admin = (): TestController =>
	t
		.typeText(usernameInput, 'testAdmin')
		.typeText(passwordInput, 'testPassword1')
		.click(loginButton);
