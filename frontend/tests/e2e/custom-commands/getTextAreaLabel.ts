export default (input: Selector): Promise<string> => {
	return input.find('label').textContent;
};
