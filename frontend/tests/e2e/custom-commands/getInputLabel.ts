export default (input: Selector): Promise<string> => {
	return input.parent().child('label').textContent;
};
