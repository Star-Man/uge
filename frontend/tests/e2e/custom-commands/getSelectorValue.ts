export default (input: Selector): Promise<string> => {
	return input.find('.v-select__selection').textContent;
};
