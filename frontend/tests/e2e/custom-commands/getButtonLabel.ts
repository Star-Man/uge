export default (input: Selector): Promise<string> => {
	return input.child('span').textContent;
};
