export default (input: Selector): Promise<string> => {
	return input.parent().parent().parent().find('.v-messages__message')
		.textContent;
};
