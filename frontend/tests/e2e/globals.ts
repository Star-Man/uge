/* eslint-disable @typescript-eslint/no-var-requires */
import { resolve } from 'path';
import { config } from 'dotenv';
import * as knex from 'knex';
import Knex from 'knex';

config({
	path: resolve(
		__dirname,
		`../../../config/${process.env.UGE_ENV || 'local'}.env`
	)
});

let db: Knex;

export const seedDb = (): Promise<[string[]]> => {
	db = knex.default({
		client: 'postgres',
		connection: {
			host: process.env.POSTGRES_HOST,
			user: process.env.POSTGRES_USER,
			password: process.env.POSTGRES_PASSWORD,
			database: process.env.POSTGRES_DB
		},
		seeds: {
			directory: './tests/e2e/seeds'
		}
	});
	return db.seed.run();
};
export const destroyDb = (): Promise<void> => db.destroy();
export const database = (): Knex => db;
export const baseUrl = `http://127.0.0.1:${process.env.FRONTEND_PORT}`;
