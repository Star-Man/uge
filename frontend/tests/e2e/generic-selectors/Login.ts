import { Selector } from 'testcafe';

export const usernameInput = Selector('#loginUsernameInput');
export const passwordInput = Selector('#loginPasswordInput');
export const loginButton = Selector('#loginButton');
