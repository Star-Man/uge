import { Selector } from 'testcafe';

export const infoSnackbar = Selector('.infoSnackbar');
export const snackbarText = Selector('.snackbarText');
export const snackbarButton = Selector('.snackbarButton');
export const registerTab = Selector('a').withAttribute('href', '#register-tab');
