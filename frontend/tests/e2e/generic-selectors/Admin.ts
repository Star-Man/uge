import { Selector } from 'testcafe';

export const userTable = Selector('#userListTable');
export const userTableRows = userTable.find('tr');
export const userTableRow1 = userTableRows.nth(1);
export const userTableRow1Col3 = userTableRow1.child('td').nth(2);
const userTableRow1Col4 = userTableRow1.child('td').nth(3);
export const userTableRow1EditButton = userTableRow1Col4.find('svg').nth(0);

export const groupTable = Selector('#groupListTable');
export const groupTableRows = groupTable.find('tr');
export const groupTableRow1 = groupTableRows.nth(1);
