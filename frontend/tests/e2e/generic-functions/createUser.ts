import axios, { AxiosResponse } from 'axios';
import * as dotenv from 'dotenv';

dotenv.config({
	path: `../../../config/${process.env.UGE_ENV || 'local'}.env`
});
const http = axios.create({
	baseURL: `${process.env.VUE_APP_API_PROTOCOL}://${process.env.VUE_APP_API_HOST}:${process.env.VUE_APP_API_PORT}${process.env.VUE_APP_API_PATH}/api/`
});

const createUser = (): Promise<AxiosResponse> =>
	http.post('registration/', {
		username: 'newTestUser',
		displayName: 'New Test User',
		password1: 'testPassword',
		password2: 'testPassword'
	});

export default createUser;
