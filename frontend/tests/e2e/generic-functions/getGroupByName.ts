import { QueryBuilder } from 'knex';
import { database } from '../globals';

const getGroupByName = (groupName: string): QueryBuilder =>
	database()
		.select()
		.from('authentication_customgroup')
		.where('name', groupName)
		.columns([
			'authentication_customgroup.id AS id',
			'authentication_customgroup.name AS name',
			'authentication_customgroup.isActive AS isActive'
		]);
export default getGroupByName;
