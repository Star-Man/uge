import axios, { AxiosResponse } from 'axios';
import * as dotenv from 'dotenv';
import getUserByName from './getUserByName';

dotenv.config({
	path: `../../../config/${process.env.UGE_ENV || 'local'}.env`
});

const http = axios.create({
	baseURL: `${process.env.VUE_APP_API_PROTOCOL}://${process.env.VUE_APP_API_HOST}:${process.env.VUE_APP_API_PORT}${process.env.VUE_APP_API_PATH}/api/`
});

// eslint-disable-next-line @typescript-eslint/ban-types
const updateUser = (username: string, data: object): Promise<AxiosResponse> =>
	// eslint-disable-next-line camelcase
	getUserByName(username).then((user: Array<{ id: number }>) =>
		http
			.post('login/', {
				username: 'testAdmin',
				password: 'testPassword1'
			})
			.then(response =>
				http.patch(`users/${user[0].id}/update/`, data, {
					headers: {
						Authorization: `Token ${response.data.key}`
					}
				})
			)
	);

export default updateUser;
