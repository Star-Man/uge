import axios, { AxiosResponse } from 'axios';
import * as dotenv from 'dotenv';

dotenv.config({
	path: `../../../config/${process.env.UGE_ENV || 'local'}.env`
});
const http = axios.create({
	baseURL: `${process.env.VUE_APP_API_PROTOCOL}://${process.env.VUE_APP_API_HOST}:${process.env.VUE_APP_API_PORT}${process.env.VUE_APP_API_PATH}/api/`
});

const createGroup = (name: string): Promise<AxiosResponse> =>
	http
		.post('login/', {
			username: 'testAdmin',
			password: 'testPassword1'
		})
		.then(response =>
			http.post(
				`groups/`,
				{ name },
				{
					headers: {
						Authorization: `Token ${response.data.key}`
					}
				}
			)
		);

export default createGroup;
