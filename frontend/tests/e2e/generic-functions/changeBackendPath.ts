import { ClientFunction } from 'testcafe';

const changeBackendPath = ClientFunction((backendPath: string) => {
	// eslint-disable-next-line no-underscore-dangle
	(
		document.getElementById(
			'app'
			// eslint-disable-next-line @typescript-eslint/no-explicit-any
		) as any
	).__vue__.$store.state.backendPath = backendPath;

	// eslint-disable-next-line no-underscore-dangle
	return (
		document.getElementById(
			'app'
			// eslint-disable-next-line @typescript-eslint/no-explicit-any
		) as any
	).__vue__.$store.commit('WS_CONNECT');
});

export default changeBackendPath;
