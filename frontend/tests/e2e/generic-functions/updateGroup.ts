import axios, { AxiosResponse } from 'axios';
import * as dotenv from 'dotenv';
import getGroupByName from './getGroupByName';

dotenv.config({
	path: `../../../config/${process.env.UGE_ENV || 'local'}.env`
});

const http = axios.create({
	baseURL: `${process.env.VUE_APP_API_PROTOCOL}://${process.env.VUE_APP_API_HOST}:${process.env.VUE_APP_API_PORT}${process.env.VUE_APP_API_PATH}/api/`
});

// eslint-disable-next-line @typescript-eslint/ban-types
const updateGroup = (groupName: string, data: object): Promise<AxiosResponse> =>
	getGroupByName(groupName).then((group: Array<{ id: number }>) =>
		http
			.post('login/', {
				username: 'testAdmin',
				password: 'testPassword1'
			})
			.then(response =>
				http.patch(`groups/${group[0].id}/update/`, data, {
					headers: {
						Authorization: `Token ${response.data.key}`
					}
				})
			)
	);

export default updateGroup;
