import axios, { AxiosResponse } from 'axios';

import * as dotenv from 'dotenv';

dotenv.config({
	path: `../../../config/${process.env.UGE_ENV || 'local'}.env`
});
const http = axios.create({
	baseURL: `${process.env.VUE_APP_API_PROTOCOL}://${process.env.VUE_APP_API_HOST}:${process.env.VUE_APP_API_PORT}${process.env.VUE_APP_API_PATH}/api/`
});

const loginRequest = (
	username: string,
	password: string
): Promise<AxiosResponse> =>
	http.post('login/', {
		username,
		password
	});

export default loginRequest;
