import { QueryBuilder } from 'knex';
import { database } from '../globals';

const getUserByName = (username: string): QueryBuilder =>
	database()
		.select()
		.from('authentication_customuser')
		.leftJoin(
			'authentication_customuser_groups',
			'authentication_customuser.id',
			'authentication_customuser_groups.customuser_id'
		)
		.leftJoin(
			'authentication_customgroup',
			'authentication_customgroup.id',
			'authentication_customuser_groups.customgroup_id'
		)
		.where('username', username)
		.columns([
			'authentication_customuser.id AS id',
			'authentication_customgroup.name AS groupName',
			'authentication_customuser.is_active AS isActive',
			'authentication_customuser.displayName AS displayName'
		]);
export default getUserByName;
