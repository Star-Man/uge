exports.seed = knex =>
	knex
		.raw('TRUNCATE TABLE authentication_customuser CASCADE')
		.then(async () => {
			await knex.raw('TRUNCATE TABLE authentication_customgroup CASCADE');
			await knex('authentication_customgroup').insert(
				[
					{
						name: 'nonAdminGroup',
						isActive: true
					},
					{
						name: 'admin',
						isActive: true
					}
				],
				['id']
			);
			const [nonAdminGroup] = await knex
				.select('id')
				.from('authentication_customgroup')
				.where({ name: 'nonAdminGroup' });
			const [adminGroup] = await knex
				.select('id')
				.from('authentication_customgroup')
				.where({ name: 'admin' });
			return knex('authentication_customuser')
				.insert(
					[
						{
							username: 'testAdmin',
							displayName: 'Test Admin',
							password:
								'pbkdf2_sha256$120000$ug2WOCLCuKse$1TINlkBwEfviiHfvgcXes1zGK5O+5YBpe0+8dXWJpeA=',
							is_superuser: false,
							first_name: '',
							last_name: '',
							is_staff: false,
							is_active: true,
							date_joined: '2020-02-26 21:17:56'
						},
						{
							username: 'nonAdminGroupUser',
							displayName: 'Test Non Admin User',
							password:
								'pbkdf2_sha256$120000$ug2WOCLCuKse$1TINlkBwEfviiHfvgcXes1zGK5O+5YBpe0+8dXWJpeA=',
							is_superuser: false,
							first_name: '',
							last_name: '',
							is_staff: false,
							is_active: true,
							date_joined: '2020-02-26 21:17:56'
						}
					],
					['id']
				)
				.then(ids =>
					knex('authentication_customuser_groups').insert([
						{
							customuser_id: ids[0].id,
							customgroup_id: adminGroup.id
						},
						{
							customuser_id: ids[1].id,
							customgroup_id: nonAdminGroup.id
						}
					])
				);
		});
