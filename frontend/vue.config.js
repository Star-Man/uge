/* eslint-disable @typescript-eslint/no-var-requires */
/* eslint-disable global-require */
const path = require('path');

require('dotenv').config({
	path: path.resolve(
		__dirname,
		`../config/${process.env.UGE_ENV || 'local'}.env`
	)
});

module.exports = {
	devServer: {
		port: process.env.FRONTEND_PORT
	},
	transpileDependencies: ['vuetify'],
	css: {
		loaderOptions: {
			sass: {
				implementation: require('sass')
			}
		}
	},
	publicPath: process.env.FRONTEND_BASE_PATH
};
