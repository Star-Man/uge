import Vue from 'vue';
import Vuex from 'vuex';
import createPersistedState from 'vuex-persistedstate';
import SecureLS from 'secure-ls';
import http from '../backend/http';

const ls = new SecureLS({ isCompression: false });
Vue.use(Vuex);
export const wsReconnect = (state: State): void => {
	Vue.prototype.$disconnect();
	Vue.prototype.$connect(
		`${process.env.VUE_APP_WS_PROTOCOL}://${state.backendPath}/ws/main/?token=${state.token}`
	);
};

const initialState = () => {
	const state: State = {
		token: null,
		http: http(),
		user: {
			id: null,
			username: null,
			displayName: null,
			groups: [],
			isActive: false
		},
		snackbar: {
			text: '',
			color: ''
		},
		topBar: {
			text: '',
			color: '',
			active: false
		},
		socket: {
			isConnected: false,
			message: '',
			reconnectError: false
		},
		users: {},
		groups: {},
		backendPath: ''
	};
	return state;
};

export default new Vuex.Store({
	plugins: [
		createPersistedState({
			storage: {
				getItem: key => ls.get(key),
				setItem: (key, value) => ls.set(key, value),
				removeItem: key => ls.remove(key)
			}
		})
	],
	mutations: {
		WS_CONNECT(state: State) {
			wsReconnect(state);
		},
		SOCKET_ONOPEN(state: State, event) {
			Vue.prototype.$socket = event.currentTarget;
			state.socket.isConnected = true;
			Vue.set(state.topBar, 'active', false);
		},
		SOCKET_ONCLOSE(state: State) {
			state.socket.isConnected = false;
		},
		SOCKET_ONERROR(state: State) {
			Vue.set(
				state.topBar,
				'text',
				'Lost connection to server, reconnecting...'
			);
			Vue.set(state.topBar, 'color', 'error');
			Vue.set(state.topBar, 'active', true);
		},
		SOCKET_ONMESSAGE(state: State, message) {
			state.socket.message = message;
		},
		SOCKET_RECONNECT_ERROR(state) {
			state.socket.reconnectError = true;
			state.snackbar = {
				text: 'There was an error reconnecting to the real-time backend server',
				color: 'error'
			};
		},
		SOCKET_RECONNECT(state) {
			state.socket.isConnected = true;
			Vue.set(state.topBar, 'active', false);
		},
		UPDATE_USER(state: State, user: User) {
			if (user.id) {
				if (!state.users[user.id]) {
					Vue.set(state.users, user.id, user);
				} else {
					Vue.set(state.users[user.id], 'username', user.username);
					Vue.set(
						state.users[user.id],
						'displayName',
						user.displayName
					);
					Vue.set(state.users[user.id], 'isActive', user.isActive);
					Vue.set(state.users[user.id], 'username', user.username);
					Vue.set(state.users[user.id], 'groups', user.groups);
				}
				Vue.set(state.users, user.id, state.users[user.id]);
				if (state.user.id === user.id) {
					Vue.set(state.user, 'username', user.username);
					Vue.set(state.user, 'displayName', user.displayName);
					Vue.set(state.user, 'isActive', user.isActive);
					if (user.groups) {
						Vue.set(state.user, 'groups', user.groups);
					}
				}
			}
		},
		UPDATE_GROUP(state: State, group: Group) {
			Vue.set(state.groups, group.id, group);
		},
		RESET_STATE(state: State) {
			Object.assign(state, initialState());
		}
	},
	state: initialState(),
	actions: {
		// Actions are directly called from the backend by websockets
		updateUser({ commit }, model) {
			commit('UPDATE_USER', model.data);
		},
		updateGroup({ commit }, model) {
			commit('UPDATE_GROUP', model.data);
		}
	}
});
