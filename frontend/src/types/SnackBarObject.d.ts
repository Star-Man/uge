/* eslint-disable @typescript-eslint/ban-types */
interface SnackBarObject {
	text: string;
	color: string;
	snackbar: boolean;
	id: number;
	buttonFunction: Function;
	afterCloseFunction: Function;
	buttonFunctionCalled: boolean;
}
