interface WSVue {
	$connect(path: string): undefined;
	$disconnect(): undefined;
}
