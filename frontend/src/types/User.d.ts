interface User {
	id: null | number;
	username: null | string;
	displayName: null | string;
	groups: Array<Group>;
	isActive: boolean;
}
