interface State {
	token: null | string;
	http: import('axios').AxiosInstance;
	user: User;
	snackbar: import('./SnackBarObject').snackbar;
	topBar: import('./SnackBarObject').snackbar;
	socket: {
		isConnected: boolean;
		message: string;
		reconnectError: boolean;
	};
	users: Record<string, User>;
	groups: Record<string, Group>;
	backendPath: string;
}
