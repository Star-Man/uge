interface Group {
	id: number;
	name: string;
	isActive: boolean;
}
