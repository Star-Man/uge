interface LoginResponse {
	data: {
		key: string;
		user: {
			id: number;
			username: string;
			displayName: string;
			groups: Array<Group>;
		};
	};
}
