import Vue from 'vue';
import VueRouter from 'vue-router';
import store from '../store/store';
import Auth from '../views/Auth.vue';
import DefaultLayout from '../layouts/DefaultLayout.vue';
import UserSettings from '../views/User.vue';
import Admin from '../views/Admin.vue';

Vue.use(VueRouter);

const isAdmin = (user: User) =>
	user.groups.some((group: Group) => group.name === 'admin');

const routes = [
	{
		path: '/auth',
		name: 'auth',
		component: Auth
	},
	{
		path: '/',
		name: 'main',
		component: DefaultLayout,
		meta: {
			requiresAuth: true
		},
		children: [
			{
				path: 'admin',
				name: 'admin',
				component: Admin,
				meta: {
					requiresAdmin: true
				}
			},
			{
				path: 'user-settings',
				name: 'userSettings',
				component: UserSettings
			}
		]
	},
	{ path: '*', redirect: '/user-settings' }
];

const router = new VueRouter({
	routes
});

router.beforeEach((to, _from, next) => {
	if (to.matched.some(record => record.meta.requiresAuth)) {
		if (store.state.token === null) {
			return next({
				name: 'auth',
				query: { nextUrl: to.fullPath }
			});
		}
		store.state.http.defaults.headers.common.Authorization = `Token ${store.state.token}`;
		const admin = isAdmin(store.state.user);
		if (to.matched.some(record => record.meta.requiresAdmin) && !admin) {
			return next({ name: 'userSettings' });
		}
	}

	if (to.name === 'auth' && store.state.token !== null) {
		return next({ name: 'userSettings' });
	}
	if (to.name === 'auth') {
		store.state.user.id = null;
		store.state.token = null;
	}
	return next();
});

export default router;
