import Vue from 'vue';
import 'vue-swatches/dist/vue-swatches.css';
import VueAxios from 'vue-axios';
import axios from 'axios';
import store from '@/store/store';
import App from './App.vue';
import router from './router';
import vuetify from './plugins/vuetify';
import websocketSetup from './backend/websockets';

Vue.config.productionTip = false;
Vue.use(VueAxios, axios);
websocketSetup();
new Vue({
	router,
	store,
	vuetify,
	render: h => h(App)
}).$mount('#app');
