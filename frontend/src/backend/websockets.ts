import path from 'path';
import Vue from 'vue';
import { config } from 'dotenv';
import store from '../store/store';
import getBackendPath from './getBackendPath';

// eslint-disable-next-line @typescript-eslint/no-var-requires
const VueNativeSock = require('vue-native-websocket').default;

config({
	path: path.resolve(
		__dirname,
		`../../../config/${process.env.UGE_ENV || 'local'}.env`
	)
});

export default (): void => {
	store.state.backendPath = getBackendPath();
	Vue.use(
		VueNativeSock,
		`${process.env.VUE_APP_WS_PROTOCOL}://${store.state.backendPath}/ws/main/?token=${store.state.token}`,
		{
			format: 'json',
			store,
			reconnection: true,
			reconnectionAttempts: 5,
			reconnectionDelay: 3000,
			connectManually: true
		}
	);
};
