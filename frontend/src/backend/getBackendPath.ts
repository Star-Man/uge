import { config } from 'dotenv';
import path from 'path';

config({
	path: path.resolve(
		__dirname,
		`../../../config/${process.env.UGE_ENV || 'local'}.env`
	)
});
export default function getBackendPath(): string {
	let host = process.env.VUE_APP_API_HOST;
	if (!process.env.VUE_APP_API_HOST) {
		host = window.location.hostname;
	}
	return `${host}:${process.env.VUE_APP_API_PORT}${process.env.VUE_APP_API_PATH}`;
}
