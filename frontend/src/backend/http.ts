import axios, { AxiosInstance } from 'axios';
import path from 'path';
import { config } from 'dotenv';
import getBackendPath from './getBackendPath';

config({
	path: path.resolve(
		__dirname,
		`../../../config/${process.env.UGE_ENV || 'local'}.env`
	)
});

export default (): AxiosInstance =>
	axios.create({
		baseURL: `${
			process.env.VUE_APP_API_PROTOCOL
		}://${getBackendPath()}/api/`
	});
