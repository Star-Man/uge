import Vue from 'vue';
import { Mixin } from 'vue-mixin-decorator';
import { AxiosResponse, AxiosError } from 'axios';

@Mixin
export default class ProxyMixin extends Vue {
	getUsers = async (): Promise<void> =>
		this.$store.state.http
			.get('users/')
			.then((response: AxiosResponse) => {
				const initialValue = {};
				this.$store.state.users = response.data.reduce(
					(userObject: Record<string, User>, user: User) => {
						if (user.id) {
							return {
								...userObject,
								[user.id.toString()]: user
							};
						}
						return userObject;
					},
					initialValue
				);
			})
			.catch(() => this.axiosFailed());

	getGroups = async (): Promise<void> =>
		this.$store.state.http
			.get('groups/')
			.then((response: AxiosResponse) => {
				const initialValue = {};
				this.$store.state.groups = response.data.reduce(
					(groupObject: Record<string, User>, group: Group) => {
						if (group.id) {
							return {
								...groupObject,
								[group.id.toString()]: group
							};
						}
						return groupObject;
					},
					initialValue
				);
			})
			.catch(() => this.axiosFailed());

	axiosFailed(err?: AxiosError): void {
		if (err && err.response && err.response.data) {
			if (err.response.data.non_field_errors) {
				this.$store.state.snackbar = {
					text: err.response.data.non_field_errors[0],
					color: 'error'
				};
			}
		} else {
			this.$store.state.snackbar = {
				text: 'There was a problem connecting to the backend server',
				color: 'error'
			};
		}
	}

	goToNextPage(): void {
		if (this.$route.query.nextUrl && this.$route.query.nextUrl !== '/') {
			this.$router
				.push(this.$route.query.nextUrl as string)
				.catch(err => err);
		} else {
			this.$router.push({ name: 'userSettings' }).catch(err => err);
		}
	}
}
