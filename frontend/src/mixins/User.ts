import Vue from 'vue';
import { Mixin, Mixins } from 'vue-mixin-decorator';
import getBackendPath from '@/backend/getBackendPath';
import Proxy from './Proxy';

@Mixin
export default class UserMixin extends Mixins<Proxy>(Proxy) {
	logout(): void {
		this.$store.commit('RESET_STATE');
		this.$router.push({ name: 'auth' });
		localStorage.clear();
		this.$store.state.backendPath = getBackendPath();
		return this.$store.commit('WS_CONNECT');
	}

	loginSuccess(response: LoginResponse): void {
		this.$store.state.token = response.data.key;
		Vue.set(this.$store.state, 'user', response.data.user);
		this.$store.state.http.defaults.headers.common.Authorization = `Token ${this.$store.state.token}`;
		this.goToNextPage();
		return this.$store.commit('WS_CONNECT');
	}
}
