# USELESS GROUP EDITOR (UGE)

[![pipeline status](https://gitlab.com/Star-Man/uge/badges/master/pipeline.svg)](https://gitlab.com/Star-Man/uge/pipelines/latest)
[![coverage](https://codecov.io/gl/Star-Man/uge/branch/master/graph/badge.svg)](https://codecov.io/gl/Star-Man/uge)
[![License: AGPL v3](https://img.shields.io/badge/License-AGPL%20v3-blue.svg)](https://www.gnu.org/licenses/agpl-3.0)
[![Maintainability Rating](https://sonarcloud.io/api/project_badges/measure?project=Star-Man_uge&metric=sqale_rating)](https://sonarcloud.io/dashboard?id=Star-Man_uge)
[![Reliability Rating](https://sonarcloud.io/api/project_badges/measure?project=Star-Man_uge&metric=reliability_rating)](https://sonarcloud.io/dashboard?id=Star-Man_uge)
[![Security Rating](https://sonarcloud.io/api/project_badges/measure?project=Star-Man_uge&metric=security_rating)](https://sonarcloud.io/dashboard?id=Star-Man_uge)


 _An almost useless application to manage users and groups_

## Features
- 100% code coverage (for no reason)
- Many e2e tests
- Login/Registration
- Set multiple groups per user
- Only admins can change groups
- Prevent deletion of admin group
- First user to register becomes the admin
- Create groups
- Rename groups (By clicking their name)
- Delete groups 
- Delete users that aren't you
- Instant updates using websockets
- Handles deleting groups that have users by not showing those groups for those users anymore
- Full pipeline with testing, code coverage and sonarcloud for some reason

## Running Locally

### Requirements

1. [PostgreSQL](https://www.postgresql.org/) installed and running on the default port (5432)
2. [Redis>5.0](https://redis.io/) installed and running on the default port (6379)
3. [Python>3.7 + Poetry](https://python-poetry.org/)
4. [Yarn](https://yarnpkg.com/getting-started/install)
5. [Git](https://git-scm.com/downloads)

### Steps

1. Clone this repo

   ```
   git clone https://gitlab.com/Star-Man/uge.git
   ```

2. Create a user and database in your postgres database that matches the user in the
   [default config file](config/local.env).

   ```
   psql -U postgres -c "CREATE DATABASE uge_db"
   psql -U postgres -c "create role db_user with password 'password' LOGIN CREATEDB;"
   ```

   <b>If you wish to use a different username/password, adjust the values in the
   [config file](config/local.env)</b>

3. Enter the backend folder

   ```
   cd uge/backend
   ```

4. Install required python packages for running the backend server

   ```
   poetry install --no-dev
   ```

5. Run migrations

   ```
   poetry run python3 manage.py migrate
   ```

6. Run the backend server

   ```
   poetry run uvicorn app.asgi:application
   ```

   <b>This will run the server locally, keep this running.</b>

7. Open a new window and enter the frontend folder
   ```
   cd uge/frontend
   ```
8. Install required node modules for running the frontend server
   ```
   yarn
   ```
9. Start the frontend server

    ```
    yarn serve
    ```

## Running With Docker

### Requirements

1. [Docker Compose](https://docs.docker.com/compose/install/)
2. [Git](https://git-scm.com/downloads)

### Steps

1. Run docker-compose command:
   ```
   docker-compose -f build/dockercompose.yaml -f build/dockercompose-local.yaml up frontend
   ```

## Configuring

Configuration of project settings can be done through the .env files in the uge/config
folder. If running the project locally, edit the [local.env](config/local.env) file,
if using docker, edit the [docker.env](config/docker.env) file.

You can also create additional .env files by changing the environment variable
`UGE_ENV`. When running locally run:

```
export UGE_ENV=prod
```

Now create a file called `prod.env` in the config folder and this file will be used
instead of `local.env`

If running docker, edit the file [build/dockercompose.yaml](build/dockercompose.yaml),
and update all cases where the `UGE_ENV` variable is passed to a service, then edit
any instances where an `env_file` parameter is used and change it to your new file.

## Deploying

### Supervisord

Install supervisord and uvicorn, below is a template setup for supervisord:
```toml
[supervisord]

[fcgi-program:uvicorn]
environment=UGE_ENV=default
socket=tcp://localhost:8000
command=uvicorn --fd 0 --app-dir PATH_TO_UGE_BACKEND app.asgi:application
numprocs=4
process_name=uvicorn-%(process_num)d
stdout_logfile=/dev/stdout
stdout_logfile_maxbytes=0
```

### NGINX

To deploy with nginx, firstly make sure the backend is running on port 8000
(or change to your taste). Then generate the `dist` folder in the frontend by running the
command below with an env of your choosing
```
UGE_ENV=default yarn build
```
Nginx Conf:
```nginx
upstream app {
    server 127.0.0.1:8000;
}

server {
    listen       80;
    charset     utf-8;
    client_max_body_size 75M;
    location /backend {
        try_files $uri @proxy_to_app;
    }
    location @proxy_to_app {
        proxy_pass http://app;

        proxy_http_version 1.1;
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection "upgrade";

        proxy_redirect off;
        proxy_set_header Host $host;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Host $server_name;
    }
    location / {
        root /uge/dist;
    }
    access_log /var/log/nginx/main;
}
```

## Contributing
You can't.
