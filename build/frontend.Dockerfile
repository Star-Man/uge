FROM node:12-alpine
RUN mkdir /app
COPY ./frontend/ /app/frontend
WORKDIR /app/frontend
RUN yarn install --frozen-lockfile
CMD yarn serve