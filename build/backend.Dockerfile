FROM python:3.8
ENV PYTHONUNBUFFERED 1
RUN mkdir /app
COPY ./backend/ /app/backend
COPY ./config/ /app/config/

WORKDIR /app/backend
RUN pip install poetry
RUN poetry config virtualenvs.create false
RUN poetry install
