FROM ubuntu:18.04
RUN apt update \
  && apt install -y unzip curl wget git make build-essential g++ openjdk-8-jdk \
  && curl -sL https://deb.nodesource.com/setup_12.x | bash - \
  && apt update \
  && apt-get install -y nodejs \
  && apt update \
  && apt install -y chromium-browser firefox

RUN mkdir /app
COPY ./frontend/ /app/frontend
WORKDIR /app/frontend
RUN npm i -g yarn
RUN yarn install --production=false --frozen-lockfile