FROM node:14-slim
RUN apt-get update || : && apt-get install python -y
RUN mkdir /app
COPY ./frontend/ /app/frontend
WORKDIR /app/frontend
RUN yarn install --production=false --frozen-lockfile
CMD yarn test:unit